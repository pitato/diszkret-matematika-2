---
title: "Legnagyobb közös osztó, legkisebb közös többszörös"
date: "2020-11-11"
tags:
  - Diszkrét Matematika 2
  - Számelmélet
---

## Legnagyobb közös osztó (Definíció)

Az $a$ és $b$ számoknak a $d$ szám legnagyobb közös osztója (kitüntetett közös osztója), ha:  
$d \mid a$, $d \mid b$, és $(c \mid a \land c \mid b) \implies c \mid d$.  
Jelölés: Legyen $(a,b) = \text{lnko}(a,b)$ a nemnegatív legnagyobb közös osztó!

Figyelem! Itt a „legnagyobb” nem a szokásos rendezésre utal:  
$12$-nek és $9$-nek legnagyobb közös osztója lesz a $−3$ is.
A legnagyobb közös osztó csak asszociáltság erejéig egyértelmű.

Hasonló módon definiálható több szám legnagyobb közös osztója is: $(a_1, a_2, \dots, a_n)$.

## Legnagyobb közös osztó kiszámolása rekurzióval (Tétel)

Legyen $a,b$ egész szám.

$(a, b) = \begin{cases} |a| &\text{ ha } b = 0 \\ (b, a \bmod b) &\text{ ha } b \ne 0 \end{cases}$

### Bizonyítás

Ha $b = 0$, akkor a tétel nyilvánvaló.  
Egyébként $a = bq + (a \bmod b)$ valamely $q$ egészre, így $a$ a $b$ és $a \bmod b$ egy egész együtthatójú lineáris kombinációja.  
Ezért $(b, a \bmod b) \mid a$, tehát $(b, a \bmod b) \mid (a, b)$.  
Hasonlóan, $a \bmod b = a - bq$ miatt $a \bmod b$ egész együtthatójú lineáris kombinációja $a$-nak és $b$-nek, így $(a, b) \mid (b, a \bmod b)$.  
Innen $(a, b) = (b, a \bmod b)$ következik.

### Példa

Számítsuk ki $(172, 62)$ értékét!

| $(a, b)$    | $a \bmod b$ |
| :-----------: | :-----------: |
| $(172, 62)$ | $48$        |
| $(62, 48)$  | $14$        |
| $(48, 14)$  | $6$         |
| $(14, 6)$   | $2$         |
| $(6, 2)$    | $0$         |

A legnagyobb közös osztó: $(172, 62) = 2$.

## Legnagyobb közös osztó általános esetben (Definíció)

Az $a_1, a_2, \dots, a_n$ számoknak egy $d$ szám legnagyobb közös osztója, ha  
$d \mid a_i\ ( 1 \le i \le n )$ és $\forall c \in \Z: c \mid a_i\ (1 \le i \le n) \implies c \mid d$.

## Legnagyobb közös osztó létezése általános esetben (Állítás)

Bármely $a_1, a_2, \dots, a_n$ egész számokra létezik $(a_1, a_2, \dots, a_n)$ és  
$(a_1, a_2, \dots, a_n) = ((\dots (a_1, a_2), \dots, a_{n - 1}),a_n)$.

## Lnko osztása (Állítás)

Bármely $a, b, c$ egész számokra $(ca, cb) = c (a, b)$.

### Bizonyítás

Ötlet: alkalmazzuk az euklideszi algoritmust $ca$-ra és $cb$-re.

## Relatív prímek (Definíció)

$(a,b) = 1$ esetén azt mondjuk, hogy a és b relatív prímek.

## Legkisebb közös többszörös (Definíció)

Az $a$ és $b$ számoknak az $m$ szám legkisebb közös többszöröse (kitüntetett közös töbszöröse), ha: $a \mid m$, $b \mid m$, és $(a \mid c \land b \mid c) \implies m \mid c$.  
Jelölés: Legyen $[a,b] = \text{lkkt} (a,b)$ a nemnegatív legkisebb közös többszörös!

---

Következő: [Euklideszi algoritmus](./euklideszi-algoritmus)
