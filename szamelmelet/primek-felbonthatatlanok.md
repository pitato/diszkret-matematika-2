---
title: "Prímek, felbonthatatlanok"
date: "2020-11-11"
tags:
  - Diszkrét Matematika 2
  - Számelmélet
---

## Felbonthatatlan számok (Definíció)

Egy nem-nulla, nem egység $a$ számot felbonthatatlannak (irreducibilisnek) nevezünk, ha $\forall b,c \in \Z: a = bc \implies b \ \text{egység} \lor c \ \text{egység}$

### Példák

- $2, -2, 3, -3, 5, -5$ felbonthatatlanok.
- $6$ nem felbonthatatlan, mert $6 = 2 \cdot 3$

## Felbonthatatlanok ekvivalens jellemzése (Állítás)

Egy nem-nulla, nem egység szám pontosan akkor felbonthatatlan, ha a
triviális osztóin kívül nincs más osztója.

## Prímek (Definíció)

Egy nem-nulla, nem egység $p$ számot prímszámnak nevezünk, ha
$p \mid ab \implies p \mid a \lor p \mid b$

### Példák

- $2, -2, 3, -3, 5, -5$ prímszámok.
- $6$ nem prímszám, mert $6 \mid 2 \cdot 3$ de $6 \nmid 2 \land 6 \nmid 3$

## Prímek és felbonthatatlanok kapcsolata (Állítás)

Minden prímszám felbonthatatlan.

### Bizonyítás

Legyen $p$ prímszám és legyen $p = ab$ egy felbontás. Igazolnunk kell, hogy $a$ vagy $b$ egység.  
Mivel $p = ab$, így $p \mid ab$, ahonnan például $p \mid a$  
Ekkor $a = pk = abk$, azaz $bk = 1$, ahonnan következik, hogy $b$ és $k$ is egység.

A fordított irány nem feltétlenül igaz:

- $\Z$-ben igaz, (lásd később).
- $\{ a + bi\sqrt5 : a,b \in \Z \}$-ben nem igaz.

---

Következő: [Maradékos osztás](./maradekos-osztas)
