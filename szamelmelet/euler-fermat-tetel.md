---
title: "Euler-Fermat tétel"
date: "2020-11-11"
tags:
  - Diszkrét Matematika 2
  - Számelmélet
---

## Euler-Fermat tétel (Tétel)

Legyen $m > 1$ egész szám, $a$ olyan egész, melyre $(a, m) = 1$. Ekkor  
$a^{\varphi (m)} \equiv 1\ (\bmod m)$

### Következmény: Fermat tétel

Legyen $p$ prímszám, $p \nmid a$. Ekkor $a^{p - 1} \equiv 1\ (\bmod p)$  
illetve tetszőleges $a$ esetén $ap \equiv a\ (\bmod p)$

### Példák

- $\varphi (6) = 2 \implies 52 = 25 \equiv 1\ (\bmod 6)$
- $\varphi (12) = 4 \implies 54 = 625 \equiv 1\ (\bmod 12 );\ 7^4 = 2401 \equiv 1\ (\bmod 12)$

Figyelem! $2^4 = 16 \equiv 4 \not\equiv 1\ (\bmod 12)$, mert $(2, 12) = 2 \ne 1$

## Euler-Fermat tétel bizonyítása

### Teljes, ill. redukált maradékrendszer lineáris transzformációi (Lemma)

Legyen $m > 1$ egész, $a_1, a_2, \dots, a_m$ teljes maradékrendszer modulo m.  
Ekkor minden $a, b$ egészre, melyre $(a, m) = 1,\ a \cdot a_1 + b, a \cdot a_2 + b, \dots, a \cdot a_m + b$ szintén teljes maradékrendszer.  
Továbbá, ha $a_1, a_2, \dots, a_{\varphi (m)}$ redukált maradékrendszer modulo m, akkor $a \cdot a_1, a \cdot a_2, \dots, a \cdot a_{\varphi (m)}$ szintén redukált maradékrendszer.

#### Bizonyítás

Tudjuk, hogy $aa_i + b \equiv aa_j + b\ (\bmod m) \iff aa_i \equiv aa_j\ (\bmod m)$  
Mivel $(a, m) = 1$, egyszerűsíthetünk $a$-val: $a_i \equiv a_j\ (\bmod m)$.  
Tehát $a \cdot a_1 + b, a \cdot a_2 + b, \dots, a \cdot a_m + b$ páronként inkongruensek.  
Mivel számuk $m$, így teljes maradékrendszert alkotnak.  
$(a_i, m) = 1 \land (a, m) = 1 \implies (a \cdot a_i, m) = 1$  
Továbbá $a \cdot a_1, a \cdot a_2, \dots, a \cdot a_{\varphi (m)}$ páronként inkongruensek, számuk $\varphi (m) \iff$ redukált maradékrendszert alkotnak.

### Bizonyítás (Euler-Fermat tétel)

Legyen $a_1, a_2, \dots, a_{\varphi (m)}$ egy redukált maradékrendszer modulo m.  
Mivel $(a, m) = 1 \implies a \cdot a_1, a \cdot a_2, \dots, a \cdot a_{\varphi (m)}$ szintén redukált maradékrendszer.  
Innen $a^{\varphi (m)} \displaystyle\prod_{j = 1}^{\varphi(m)} a_j = \displaystyle\prod_{j = 1}^{\varphi(m)} a \cdot a_j \equiv \displaystyle\prod_{j = 1}^{\varphi(m)} a_j\ (\bmod m)$  
Mivel $\displaystyle\prod_{j = 1}^{\varphi(m)} a_j$ relatív prím $m$-hez, így egyszerűsíthetünk vele:  
$a^{\varphi (m)} \equiv 1 (\bmod m)$

### Példák

- Mi lesz a $3^{111}$ utolsó számjegye tízes számrendszerben?  
  Mi lesz $3^{111} \bmod 10$?  
  $\varphi (10) = 4 \implies 3^{111} = 3^{4 \cdot 27 + 3} = (3^4)^{27} \cdot 3^3 \equiv 1^{27} \cdot 3^3 = 3^3 = 27 \equiv 7\ (\bmod 10)$
- Oldjuk meg a $2x \equiv 5\ (\bmod 7)$ kongruenciát!  
  $\varphi (7) = 6$  
  Szorozzuk be mindkét oldalt $2^5$-nel.  
  Ekkor $5 \cdot 2^5 \equiv 2^6 x \equiv x\ (\bmod 7)$  
  És itt $5 \cdot 2^5 = 5 \cdot 32 \equiv 5 \cdot 4 = 20 \equiv 6\ (\bmod 7)$
- Oldjuk meg a $23x \equiv 4\ (\bmod 211)$ kongruenciát!  
  $\varphi (211) = 210$  
  Szorozzuk be mindkét oldalt $23^{209}$-nel.  
  Ekkor $4 \cdot 23^{209} \equiv 23^{210} x \equiv x\ (\bmod 211 )$  
  És itt $4 \cdot 23^{209} \equiv \dots (mod 211 )$

---

Következő: [RSA](./rsa)
