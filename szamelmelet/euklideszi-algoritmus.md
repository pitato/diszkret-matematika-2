---
title: "Euklideszi algoritmus"
date: "2020-11-11"
tags:
  - Diszkrét Matematika 2
  - Számelmélet
---

## Euklideszi algoritmus az egészek körében (Tétel)

Bármely két egész számnak létezik legnagyobb közös osztója, és ez meghatározható az euklideszi algoritmussal.

### Bizonyítás

Ha valamelyik szám $0$, akkor a legnagyobb közös osztó a másik szám.  
Tfh $a, b$ nem-nulla számok. Végezzük el a következő osztásokat:  
$a = bq_1 + r_1,\ 0 < r_1 < |b|$  
$b = r_1 q_2 + r_2,\ 0 < r_2 < r_1$  
$r_1 = r_2 q_3 + r_3,\ 0 < r_3 < r_2$  
$\dots$  
$r_{n - 2} = r_{n - 1} q_n + r_n,\ 0 < r_n < r_{n - 1}$  
$r_{n - 1} = r_n q_{n + 1}$

Ekkor az lnko az utolsó nem-nulla maradék: $(a,b) = r_n$.  
Itt $a = r_{-1},\ b = r_0$

Az algoritmus véges sok lépésben véget ér: $|b| > r_1 > r_2 > \dots > 0$

Az $r_n$ maradék közös osztó:  
$r_n \mid r_{n - 1} \implies r_n \mid r_{n - 1} q_n + r_n = r_{n - 2} \implies \dots \implies r_n \mid b \implies r_n \mid a$

Az $r_n$ maradék a legnagyobb közös osztó:  
Legyen $c \mid a, c \mid b \implies c \mid a - bq_1 = r_1 \implies c \mid b - r_1 q_2 = r_2 \implies \dots \implies c | r_{n - 2} - r_{n - 1} q_n = r_n$

### Példa

Számítsuk ki $(172 , 62)$ értékét!

| $i$  | $r_i$ | $q_i$ | $r_{i - 2} = r_{i - 1} q_i + r_i$ |
| ------ | ------- | ------- | ----------------------------------- |
| $-1$ | $172$ | $-$   | $-$                               |
| $0$  | $62$  | $-$   | $-$                               |
| $1$  | $48$  | $2$   | $172 = 62 \cdot 2 + 48$           |
| $2$  | $14$  | $1$   | $62 = 48 \cdot 1 + 14$            |
| $3$  | $6$   | $3$   | $48 = 14 \cdot 3 + 6$             |
| $4$  | $2$   | $2$   | $14 = 6 \cdot 2 + 2$              |
| $5$  | $0$   | $3$   | $6 = 2 \cdot 3 + 0$               |

A legnagyobb közös osztó: $(172, 62) = 2$

---

Következő: [Bővített euklideszi algoritmus](./bovitett-euklideszi-algoritmus)
