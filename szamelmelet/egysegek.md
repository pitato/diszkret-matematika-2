---
title: "Egységek"
date: "2020-11-11"
tags:
  - Diszkrét Matematika 2
  - Számelmélet
---

## Egységek (Definíció)

Ha egy $\varepsilon$ szám bármely másiknak osztója, akkor $\varepsilon$-t egységnek nevezzük.

## Egységek az egészek körében (Állítás)

Az egész számok körében két egység van: $-1, 1$.

### Bizonyítás

A $\pm 1$ nyilván egység.  
Megfordítva: ha $\varepsilon$ egység, akkor $1 = \varepsilon \cdot q$ valamely $q$ egész számra.  
Mivel $| \varepsilon |\ge 1, | q |\ge 1 \implies | \varepsilon |= 1$ , azaz $\varepsilon = \pm 1$.

### Példa

- A Gauss-egészek körében az $i$ is egység: $a + bi = i (b − ai)$.

## Egységek ekvivalens jellemzése (Állítás)

Pontosan $1$ osztói az egységek.

---

Következő: [Asszociáltak](./asszocialtak)
