---
title: "Szimultán kongruenciák"
date: "2020-11-11"
tags:
  - Diszkrét Matematika 2
  - Számelmélet
---

Szeretnénk olyan $x$ egészet, mely egyszerre elégíti ki a következő kongruenciákat:  
$2x \equiv 1\ (\bmod 3)$  
$4x \equiv 3\ (\bmod 5)$

A kongruenciákat külön megoldva:  
$x \equiv 2\ (\bmod 3)$  
$x \equiv 2\ (\bmod 5)$

Látszik, hogy $x = 2$ megoldás lesz!

Vannak-e más megoldások?  
$2, 17, 32, \dots, 2 + 15 l$

További megoldások?  
Hogyan oldjuk meg az általános esetben?

## Feladat

Oldjuk meg a következő kongruenciarendszert:  
$a_1x \equiv b_1\ (\bmod m_1)$  
$a_2x \equiv b_2\ (\bmod m_2)$  
$\vdots$  
$a_nx \equiv b_n\ (\bmod m_n)$

Az egyes $a_ix \equiv b_i\ (\bmod m_i)$ lineáris kongruenciák külön megoldhatóak.

Feltehető, hogy az $m_1, m_2, \cdots, m_n$ modulusok páronként relatív prímek, mert az általános eset mindig visszavezethető erre az esetre.

## Kínai maradéktétel (Tétel)

Legyenek $1 < m_1, m_2 , \dots, m_n$ páronként relatív prím számok, $c_1, c_2, \dots, c_n$ egészek. Ekkor az  
$x \equiv c_1\ (\bmod m_1)$  
$x \equiv c_2\ (\bmod m_2 )$  
$\vdots$  
$x \equiv c_n\ (\bmod m_n)$  
kongruenciarendszer megoldható, és bármely két megoldás kongruens
egymással modulo $m_1 \cdot m_2 \cdots m_n$.

$x \equiv c_1\ (\bmod m_1), x \equiv c_2\ (\bmod m_2), \dots, x \equiv cn (mod mn )$, $x = ?$

### Bizonyítás

A bizonyítás konstruktív!  
Legyen $m = m_1 m_2$.  
A bővített euklideszi algoritmussal oldjuk meg az $m_1 x_1 + m_2 x_2 = 1$ egyenletet.  
Legyen $c_{1, 2} = m_1 x_1 c_2 + m_2 x_2 c_1$  
Ekkor $c_{1, 2} \equiv c_j\ (\bmod m_j)\ (j = 1, 2)$ (Miért?)  
Ha $x \equiv c_{1, 2}\ (\bmod m)$, akkor $x$ megoldása az első két kongruenciának.  
Megfordítva: ha $x$ megoldása az első két kongruenciának, akkor $x - c_{1, 2}$ osztható $m_1$-gyel, $m_2$ -vel, így a
szorzatukkal is: $x \equiv c_{1, 2}\ (\bmod m)$, mivel $m_1$ és $m_2$ relatív prímek.  
Az eredeti kongruenciarendszer ekvivalens az  
$x \equiv c_{1, 2}\ (\bmod m_1 m_2)$  
$x \equiv c_2\ (\bmod m_2 )$  
$\vdots$  
$x \equiv c_n\ (\bmod m_n)$  
kongruenciarendszerrel. $n$ szerinti indukcióval adódik az állítás.

### Példa

$x \equiv 2\ (\bmod 3)$
$x \equiv 3\ (\bmod 5)$

Oldjuk meg az $3x_1 + 5x_2 = 1$ egyenletet!  
Megoldások: $x_1 = 2,\ x_2 = -1 \implies c_{1, 2} = 3 \cdot 2 \cdot 3 + 5 \cdot (- 1) \cdot 2 = 18 - 10 = 8$  
Összes megoldás: $\{8 + 15l : l \in \Z\}$

### Példa

$x \equiv 2\ (\bmod 3)$  
$x \equiv 3\ (\bmod 5)$  
$x \equiv 4\ (\bmod 7)$  
$\overset{c_{1, 2} = 8}{\implies}$  
$x \equiv 8\ (\bmod 15)$  
$x \equiv 4\ (\bmod 7)$

Oldjuk meg a $15x_{1, 2} + 7x_3 = 1$ egyenletet!  
Megoldások: $x_{1, 2} = 1, x_3 = -2 \implies c 1 , 2 , 3 = 15 \cdot 1 \cdot 4 + 7 \cdot (- 2) \cdot 8 = 60 - 112 = -52$  
Összes megoldás:$\{- 52 + 105l : l \in \Z\} = \{ 53 + 105 l : l \in \Z\}$

---

Következő: [Maradékrendszerek](./maradekrendszerek)
