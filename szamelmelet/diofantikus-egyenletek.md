---
title: "Diofantikus egyenletek"
date: "2020-11-11"
tags:
  - Diszkrét Matematika 2
  - Számelmélet
---

Diofantikus egyenletek: egyenletek egész megoldásait keressük.  
Kétváltozós lineáris diofantikus egyenlet: $ax + by = c$, ahol $a, b, c$ egészek adottak, valamint $x, y$ egészek ismeretlenek.

## Kétvált. lin. diofant. egyelet megoldhatósága (Tétel)

Az $ax + by = c$ diofantikus egyenlet pontosan akkor oldható meg, ha
$(a, b) \mid c$.  
A bővített euklideszi algoritmus segítségével megadható egy megoldás.

### Bizonyítás

$\implies$: Mivel $(a, b)$ osztója $a$-nak és $b$-nek, ezért tetszőleges lineáris kombinációjuknak is, így $x, y \in \Z$ esetén $ax + by$-nak is, ami egyenlő $c$-vel, ha $(x, y)$ megoldás.

$\impliedby$: A bővített euklideszi algoritmus segítségével megadható olyan $x' ,y' \in \Z$, hogy $ax' + by' = (a, b)$.  
Mindkét oldalt $\frac{c}{(a, b)} \in \Z$-val szorozva az $a \frac{x' c}{(a, b)} + b \frac{y' c}{(a, b)} = c$ egyenletet kapjuk, amiből leolvasható az $x_0 = \frac{x' c}{(a, b)},\ y_0 = \frac{y' c}{(a, b)}$ megoldása az egyenletnek.

## Kétváltozós lineáris diofantikus egyenlet összes megoldása (Tétel)

Ha az $ax + by = c$ diofantikus egyenletnek $(x_0, y_0)$ megoldása, akkor az összes megoldás megadható a következő alakban:  
($t \in \Z$)  
$x_t = x_0 + \frac{b}{(a, b)} t$  
$y_t = y_0 - \frac{a}{(a, b)} t$

### Bizonyítás

$ax_t + by_t = ax_0 + \frac{ab}{(a, b)} t + by_0 - \frac{ab}{(a, b)} t = ax_0 + by_0 = c$, így ezek tényleg megoldások.  
Legyenek $(x_0, y_0)$ és $(x', y')$ megoldások.  
Ekkor $ax_0 + by_0 = c = ax' + by'$, amiből $a (x' - x_0) = b (y_0 - y')$, így $b \mid a (x' - x_0)$ , továbbá $\frac{b}{(a, b)} \mid \frac{a}{(a, b)} (x' - x_0)$.  
Mivel $(\frac{b}{(a, b)}, \frac{a}{(a, b)}) = 1$ (Miért?), ezért a korábbi állítás értelmében $\frac{b}{(a, b)} \mid (x' - x_0)$.  
Tehát $x' - x_0 = \frac{b}{(a, b)} t$, azaz $x' = x_0 + \frac{b}{(a, b)} t$ valamely $t \in \Z$-re.  
Behelyettesítve $ax' + by' = c$-be adódik $y'= y_0 - \frac{a}{(a, b)} t$.

### Példa

Oldjuk meg a $172x + 62y = 6$ egyenletet az egész számok halmazán!  
$(172, 62) = 2 \mid 6$ , ezért van megoldás. A bővített euklideszi algoritmus alapján:  
$2 = 172 \cdot (-9) + 62 \cdot 25\ / \cdot 3$  
$6 = 172 \cdot (-27) + 62 \cdot 75$  
$x_0 = -27,\ y_0 = 75$  
$x_t = -27 + 31 \cdot t$  
$y_t = 75 - 86 \cdot t$  
ahol $t \in \Z$ tetszőleges.

## $\Z$-beli felbonthatatlan számok (Tétel)

$\Z$-ben minden felbonthatatlan szám prímszám.

### Bizonyítás

Legyen $p$ felbonthatatlan, és legyen $p \mid ab$.  
Tfh. $p \nmid b$.  
Ekkor $p$ és $b$ relatív prímek (Miért?). A bővített euklideszi algoritmussal kaphatunk $x, y$ egészeket, hogy $px + by = 1$.  
Innen $pax + aby = a$.  
Mivel $p$ osztója a bal oldalnak, így osztója a jobb oldalnak is: $p \mid a$.

---

Következő: [Számelmélet alaptétele](./szamelmelet-alaptetele)
