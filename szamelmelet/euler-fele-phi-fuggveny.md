---
title: "Euler-féle phi függvény"
date: "2020-11-11"
tags:
  - Diszkrét Matematika 2
  - Számelmélet
---

## Euler-féle phi függvény (Definíció)

Egy $m > 0$ egész szám esetén legyen $\varphi (m)$ az $m$-nél kisebb, hozzá relatív prím természetes számok száma $\varphi ( m ) =|\{ j : 0 \le j < m,\ (m, j) = 1 \}|$.

### Példák

- $\varphi (5) = 4 : 5$-höz relatív prím természetes számok: $1, 2, 3, 4$
- $\varphi (6) = 2 : 6$-hoz relatív prím természetes számok: $1, 5$
- $\varphi (12) = 4 : 12$-höz relatív prím természetes számok:  $1, 5, 7, 11$
- $\varphi (15) = 8 : 15$-höz relatív prím természetes számok: $1, 2, 4, 7, 8, 11, 13, 14$

### Megjegyzés

$\varphi (m)$ a redukált maradékosztályok száma modulo m.

## phi (m) kiszámítása m kanonikus alakjából (Tétel)

Legyen $m$ kanonikus alakja $m = p_1^{\alpha_1} p_2^{\alpha_2} \cdots p^l\alpha^l$  
Ekkor  
$\varphi (m) = m \cdot \displaystyle\prod_{i = 1}^l (1 - \frac{1}{p_i}) = \displaystyle\prod_{i = 1}^l (p_i^{\alpha_i} - p_i^{\alpha_{i - 1}})$

### Példák

- $\varphi (5) = 5 (1 - \frac{1}{5}) = 5^1 - 5^0 = 4$
- $\varphi (6) = 6 (1 - \frac{1}{2}) (1 - \frac{1}{3}) = (2^1 - 2^0) (3^1 - 3^0) = 2$
- $\varphi (12) = 12 (1 - \frac{1}{2}) (1 - \frac{1}{3}) = (2^2 - 2^1) (3^1 - 3^0) = 4$
- $\varphi (15) = 15 (1 - \frac{1}{3}) (1 - \frac{1}{5}) = (3^1 - 3^0) (5^1 - 5^0) = 8$

---

Következő: [Euler-Fermat tétel](./euler-fermat-tetel)
