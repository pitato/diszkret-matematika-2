---
title: "Oszthatóság"
date: "2020-11-11"
tags:
  - Diszkrét Matematika 2
  - Számelmélet
---

Ha $a$ és $b$ racionális számok ($b \ne 0$), akkor az $a/b$ osztás mindig elvégezhető (és az eredmény szintén racionális).  
Ha $a$ és $b$ egész számok, az $a/b$ osztás nem mindig végezhető el (a hányados nem feltétlenül lesz egész).

## Oszthatóság (Definíció)

Az $a$ egész osztja a $b$ egészet ($b$ osztható $a$-val): $a \mid b$, ha létezik olyan $c$ egész, mellyel $a \cdot c = b$ (azaz $a \ne 0$ esetén $b/a$ szintén egész).

### Példák

- $1 \mid 13$, mert $1 \cdot 13 = 13$
- $1 \mid n$, mert $1 \cdot n = n$
- $6 \mid 12$, mert $6 \cdot 2 = 12$
- $- 6 \mid 12$, mert $(-6) \cdot (-2) = 12$

A definíció kiterjeszthető például a Gauss-egészekre: $\{ a + bi : a,b \in \Z\}$.  

### Példák Gauss-egészekkel

- $i \mid 13$, mert $i \cdot (-13 i) = 13$
- $1 + i \mid 2$, mert $(1 + i) \cdot (1 − i) = 2$

## Oszthatóság alaptulajdonságai (Állítás)

Minden $a, b, c, d, i, b_i, c_i \in \Z$ esetén

1. $a \mid a$ (reflexív)
2. $a \mid b \land b \mid c \implies a\mid c$ (tranzitív)
3. $a \mid b \land b \mid a \implies a = \pm b$
4. $a \mid b \land c \mid d \implies ac \mid bd$
5. $a \mid b \implies ac \mid bc$
6. $ac \mid bc \land c \ne 0 \implies a \mid b$
7. $a \mid b_1, b_2,\dots, b_d \implies a \mid c_1b_1 + \dots + c_db_d$
8. $a \mid 0$, ugyanis $a \cdot 0 = 0$
9. $0 \mid a \iff a = 0$
10. $1 \mid a$ és $-1 \mid a$

---

Következő: [Egységek](./egysegek)
