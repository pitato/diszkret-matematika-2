---
title: "Bővített euklideszi algoritmus"
date: "2020-11-11"
tags:
  - Diszkrét Matematika 2
  - Számelmélet
---

## Bővített euklideszi algoritmus (Tétel)

Minden $a, b$ egész szám esetén léteznek $x, y$ egészek, hogy  
$(a, b) = x \cdot a + y \cdot b$

### Bizonyítás

Legyenek $q_i, r_i$ az euklideszi algoritmussal megkapott hányadosok, maradékok.  
Legyen $x_{-1} = 1, x_0 = 0$ és $i \ge 1$ esetén legyen $x_i = x_{i - 2} - q_i x_{i - 1}$,  
továbbá $y_{-1} = 0$, $y_0 = 1$ és $i \ge 1$ esetén legyen $y_i = y_{i - 2} - q_i y_{i - 1}$  
Teljes indukcióval bebizonyítjuk, hogy $r_{-1} = a$ és $r_0 = b$ jelöléssel $i \ge -1$ esetén  
$r_i = x_ia + y_ib$  
$i = -1$-re $a = 1 \cdot a + 0 \cdot b, i = 0$-ra $b = 0 \cdot a + 1 \cdot b$  
Feltéve, hogy $i$-nél kisebb értékekre teljesül az összefüggés az euklideszi algoritmus $i$-edik sora alapján:  
$r_i = r_{i - 2} - q_i r_{i - 1} = x_{i - 2} a + y_{i - 2} b - q_i (x_{i - 1} a + y_{i - 1} b) = (x_{i - 2} - q_i x_{i - 1}) a + (y_{i - 2} - q_i y_{i - 1}) b = x_i \cdot a + y_i \cdot b$  
Speciálisan $x_n a + y_n b = r_n = (a, b)$

### Algoritmus

- $r_{i - 2} = r_{i - 1} q_i + r_i$
- $x_{-1} = 1,\ x_0 = 0,\ x_i = x_{i - 2} - q_i x_{i - 1}$
- $y_{-1} = 0,\ y_0 = 1,\ y_i = y_{i - 2} - q_i y_{i - 1}$

### Példa

Számítsuk ki $(172, 62)$ értékét, és oldjuk meg a $172x + 62y = (172, 62)$ egyenletet!

| $i$  | $r_i$ | $q_i$ | $x_i$ | $y_i$ | $r_i = 172x_i + 62y_i$              |
| ---- | ----- | ----- | ----- | ----- | ----------------------------------- |
| $-1$ | $172$ | $-$   | $1$   | $0$   | $172 = 172 \cdot 1 + 62 \cdot 0$    |
| $0$  | $62$  | $-$   | $0$   | $1$   | $62 = 172 \cdot 0 + 62 \cdot 1$     |
| $1$  | $48$  | $2$   | $1$   | $-2$  | $48 = 172 \cdot 1 + 62 \cdot (-2)$  |
| $2$  | $14$  | $1$   | $-1$  | $3$   | $14 = 172 \cdot (-1) + 62 \cdot 3$  |
| $3$  | $6$   | $3$   | $4$   | $-11$ | $6 = 172 \cdot 4 + 62 \cdot (-11)$  |
| $4$  | $2$   | $2$   | $-9$  | $25$  | $2 = 172 \cdot (-9) + 62 \cdot 25$  |
| $5$  | $0$   | $3$   | $-$   | $-$   | $-$                                 |

A felírás: $2 = 172 \cdot (-9) + 62 \cdot 25,\ x =-9, y =25$.

## Oszthatóság felbontása relatív prímmel (Állítás)

$\forall a,b,c \in \Z: (a \mid bc \land (a, b) = 1) \implies a \mid c$

### Bizonyítás

A bővített euklideszi algoritmus alapján létezik $x,y \in \Z$, hogy $1 = xa + yb$, így  
$c = xac + ybc = (xc) \cdot a + y \cdot (bc)$  
Az oszthatóság lineáris kombinációra vonatkozó tulajdonsága alapján $a \mid c$

---

Következő: [Diofantikus egyenletek](./diofantikus-egyenletek)
