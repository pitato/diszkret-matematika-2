---
title: "Számrendszerek"
date: "2020-11-11"
tags:
  - Diszkrét Matematika 2
  - Számelmélet
---

$10$-es számrendszerben a $123$:  
$123 = 100 + 20 + 3 = 1 \cdot 10^2 + 2 \cdot 10^1 + 3 \cdot 10^0$

$2$-es számrendszerben a $123$:  
$1111011_{(2)} = 1 \cdot 2^6 + 1 \cdot 2^5 + 1 \cdot 2^4 + 1 \cdot 23 + 0 \cdot 2^2 + 1 \cdot 2^1 + 1 \cdot 2^0$  
$1111011_{(2)} = 1 \cdot 64 + 1 \cdot 32 + 1 \cdot 16 + 1 \cdot 8 + 0 \cdot 4 + 1 \cdot 2 + 1 \cdot 1$

## Számok felírása különböző számrendszerekben (Tétel)

Legyen $b > 1$ rögzített egész. Ekkor bármely $n$ pozitív egész egyértelműen felírható  
$n = \displaystyle\sum_{i = 0}^k a_ib^i$ alakban, ahol $0 \le a_i < b$ egészek, $a_k \ne 0$.

- Ez a felírás $n$ $b$ alapú számrendszerben történő felírása.
- $b$ a számrendszer alapja.
- $a_0, \dots, a_k$ az $n$ jegyei.
- $k = \lfloor log_b n \rfloor$.
- $n$ felírása $b$ alapú számrendszerben: $n = \displaystyle\sum_{i = 0}^k a_ib^i$

### Bizonyítás

A tételt indukcióval bizonyítjuk.

1. $n < b$ esetén $a_0 = n$ választással $n = a_0 b^0$.  
   A felírás egyértelműsége triviális (Miért? $b^0 = 1$).
2. Legyen $n \ge b$ és tfh. az állítás igaz minden $n$-nél kisebb pozitív egészre.  
   Legyen $r$ és $q$ az $n$-nek $b$-vel vett osztási maradéka, ill. hányadosa $(n = bq + r)$.  
   Mivel $1 \le q < n$, az indukciós feltevés alapján $q$ felírható a kívánt $q = \sum_{i = 1}^k a_ib^{i - 1}$ alakban.  
   Ekkor $a_0 = r$ választással $n = bq + r = \sum_{i = 1}^k a_ib^i + a_0 = \sum_{i = 0}^k a_ib^i$ az $n$ felírása.  

   Az egyértelműséghez vegyük észre, hogy $n$ bármely $n = \sum_{i = 0}^k a_ib^i$ felírása esetén $a_0 = r$, ami egyértelmű.  
   A többi „jegy" egyérteműsége abból következik, hogy  
   $q = (n - r) : b = (\sum_{i = 0}^k a_ib^i - a_0) : b = \sum_{i = 1}^k a_ib^{i-1}$ a $q$ egy felírása $b$ alapú számrendszerben, ami az indukciós feltevés alapján egyértelmű.

Az előbbi bizonyítás módszert is ad a felírásra.

### Példa

- Írjuk fel az $n = 123$ $10$-es számrendszerben felírt számot $2$-es számrendszerben.

| $i$ | $n$   | $n \bmod 2$ | $\frac{n - a_i}{2}$ | jegyek      |
| ----- | :-----: | :-----------: | --------------------- | ----------: |
| $0$ | $123$ | $1$         | $\frac{123 - 1}{2}$ |       $1$ |
| $1$ | $61$  | $1$         | $\frac{61 - 1}{2}$  |      $11$ |
| $2$ | $30$  | $0$         | $\frac{30 - 0}{2}$  |     $011$ |
| $3$ | $15$  | $1$         | $\frac{15 - 1}{2}$  |    $1011$ |
| $4$ | $7$   | $1$         | $\frac{7 - 1}{2}$   |   $11011$ |
| $5$ | $3$   | $1$         | $\frac{3 - 1}{2}$   |  $111011$ |
| $6$ | $1$   | $1$         | $\frac{1 - 1}{2}$   | $1111011$ |

---

Következő: [Legnagyobb közös osztó](./legnagyobb-kozos-oszto)
