---
title: "Asszociáltak"
date: "2020-11-11"
tags:
  - Diszkrét Matematika 2
  - Számelmélet
---

Oszthatóság szempontjából nincs különbség a $12$ ill.$−12$ között.

## Asszociáltak (Definíció)

$a$ és $b$ számok asszociáltak, ha $a \mid b$ és $b \mid a$.

## Asszociáltak ekvivalens jellemzése (Állítás)

Két szám pontosan akkor asszociált, ha egymás egységszeresei.

### Bizonyítás

$\implies$:  
Legyen $b = ab_1$ és $a = ba_1$.  
Ekkor $b = ab_1 = ba_1b_1$, így $a_1b_1 = 1$, vagyis $a_1$ és $b_1$ is egységek.  
$\impliedby$:  
Ha $b = ca$ és $a = db$, ahol $c, d$ egységek, akkor $a \mid b$ és $b \mid a$ nyilvánvaló.

## Triviális osztók (Definíció)

Egy számnak az asszociáltjai és az egységek a triviális osztói.

---

Következő: [Prímek, felbonthatatlanok](./primek-felbonthatatlanok)
