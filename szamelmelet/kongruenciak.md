---
title: "Kongruenciák"
date: "2020-11-11"
tags:
  - Diszkrét Matematika 2
  - Számelmélet
---

Oszthatósági kérdésekben sokszor csak a maradékos osztás esetén kapott maradék fontos:

- hét napjai
- órák száma

$16 \bmod 3 = 1,\ 4 \bmod 3 = 1\ :\ 3$-mal való oszthatóság esetén
$16 \text{"="} 4$

## Kongruencia relációk (Definíció)

Legyenek $a, b, m$ egészek, ekkor

- $a \equiv b\ (\bmod m)$ ($a$ és $b$ kongruensek modulo $m$), ha $m \mid a - b$
- $a \not\equiv b\ (\bmod m)$ ($a$ és $b$ inkongruensek), ha $m \nmid a - b$

Ekvivalens megfogalmazás: $a \equiv b\ (\bmod m) \iff a \bmod m = b \bmod m$,  
azaz $m$-mel osztva ugyanazt az osztási maradékot adják.

### Példák

- $16 \equiv 4 (\bmod 3) \text{ ui. } 3 \mid 16 - 4 \iff 16 \bmod 3 = 1 = 4 \bmod 3$
- $16 \equiv 4 (\bmod 2) \text{ ui. } 2| 16 - 4 \iff 16 \bmod 2 = 0 = 4 \bmod 2$
- $16 \not\equiv 4 (\bmod 5) \text{ ui. } 5 \nmid 16 - 4 \iff 16 \bmod 5 = 16 = 4 = 4 \bmod 5$

## A kongruenciák néhány alaptulajdonsága (Tétel)

Minden $a, b, c, d, m, m'$ egész számra igaz:

1. $a \equiv a\ (\bmod m)$ (reflexivitás)
2. $a \equiv b\ (\bmod m) \implies b \equiv a\ (\bmod m)$ (szimmetria)
3. $a \equiv b\ (\bmod m) \land b \equiv c\ (\bmod m) \implies a \equiv c\ (\bmod m)$ (tranzitivitás)
4. $a \equiv b\ (\bmod m) \land c \equiv d (\bmod m) \implies a + c \equiv b + d\ (\bmod m)$
5. $a \equiv b\ (\bmod m) \land c \equiv d\ (\bmod m) \implies ac \equiv bd\ (\bmod m)$
6. $a \equiv b\ (\bmod m) \land m' \mid m \implies a \equiv b\ (\bmod m')$

### Bizonyítás

1. $m \mid 0 = a - a$
2. $m \mid a - b \implies m \mid b - a = -(a - b)$
3. $m \mid a - b \land m \mid b - c \implies m \mid a - c = (a - b) + (b - c)$
4. $m \mid a - b \land m \mid c - d \implies m \mid (a + c) - (b + d) = (a - b) + (c - d)$
5. $a = q_1 m + b \land c = q_2 m + d \implies ac = (q_1 m + b)(q_2 m + d) = m (q_1 q_2 m + q_1 d + q_2 b) + bd$
6. $m' \mid m \mid a - b \implies m' | a - b$

## Kongruencia tulajdonságai: maradékosztályok

Az előbbi tétel 1., 2. és 3. pontjai alapján tetszőleges $m$ egész esetén a modulo $m$ kongruencia ($\equiv$) ekvivalenciareláció $\Z$-n.  
Ennek ekvivalenciaosztályait modulo m maradékosztályoknak nevezzük.

## Maradékosztályok modulo m (Definíció)

Egy rögzített $m$ modulus és $a$ egész esetén, az $a$-val kongruens elemek halmazát az $a$ által reprezentált maradékosztálynak nevezzük:  
$\bar{a} = \{x \in \Z: x \equiv a\ (\bmod m)\}=\{ a + lm : l \in \Z\}$

### Megjegyzések

- Két szám pontosan akkor tartozik ugyanahhoz a $\bmod m$ maradékosztályhoz, ha $m$-mel való osztási maradékuk megegyezik.
- A $\bmod m$ maradékosztályok száma $m$.

### Példa

Mi lesz $345 \bmod 7 =$ ?  
$345 = 34 \cdot 10 + 5 \equiv 6 \cdot 3 + 5 = 18 + 5 \equiv 4 + 5 = 9 \equiv 2\ (\bmod 7)$

## Kongruencia osztása (Tétel)

Legyenek $a, b, c, m$ egész számok.  
Ekkor $ac \equiv bc\ (\bmod m) \iff a \equiv b\ (\bmod \frac{m}{(c, m)})$

### Bizonyítás

Legyen $d = (c, m)$  
Ekkor $ac \equiv bc\ (\bmod m) \iff m \mid c (a - b) \iff \frac{m}{d} \mid \frac{c}{d} (a - b)$  
Mivel $(\frac{m}{d}, \frac{c}{d}) = 1$  
ezért $\frac{m}{d} \mid \frac{c}{d} (a - b) \iff \frac{m}{d} \mid ( a - b ) \iff a \equiv b\ (\bmod \frac{m}{d})$

### Következmény

$(c, m) = 1$ esetén $ac \equiv bc\ (\bmod m) \iff a \equiv b\ (\bmod m)$

### Példa

$14 \equiv 6\ (\bmod 8) \implies 42 \equiv 18\ (\bmod 8)$  
A másik irány nem igaz!  
$2 \cdot 7 \equiv 2 \cdot 3\ (\bmod 8) \nRightarrow 7 \equiv 3\ (\bmod 8)$  
$2 \cdot 7 \equiv 2 \cdot 3 (\bmod 8) \implies 7 \equiv 3\ (mod \frac{8}{2})$

---

Következő: [Lineáris kongruenciák](./linearis-kongruenciak)
