---
title: "Maradékos osztás"
date: "2020-11-11"
tags:
  - Diszkrét Matematika 2
  - Számelmélet
---

A számelméletben a fő eszközünk a maradékos osztás lesz:

## Maradékos osztás tétele az egész számok körében (Tétel)

Tetszőleges $a$ egész számhoz és $b \ne 0$ egész számhoz egyértelműen léteznek $q, r$ egészek, hogy  
$a = bq + r$ és $0 \le r < | b |$.

### Bizonyítás

A tételt csak nemnegatív számok esetében bizonyítjuk.

1. Létezés: $a$ szerinti indukcióval.
   - Ha $1 \le a < b$, akkor $a = b \cdot 0 + a\ (q = 0, r = a)$.
   - Legyen $a \ge b$ és tegyük fel, hogy az $a$-nál kisebb számok mind felírhatók ilyen alakban. Az indukciós feltevés értelmében $a - b = bq^∗ + r^∗$.  
   Ekkor $a = b (q^∗ + 1) + r^∗$ ($q = q^∗ + 1, r = r^∗$).
2. Egyértelműség: indirekt  
   Legyen $a = bq_1 + r_1 = bq_2 + r_2$ valamely $q_1, q_2, r_1, r_2$ egészekre, ahol $0 \le r_1, r_2 < b$.  
   Tf. indirekt, hogy $q_1 \ne q_2$.  
   Ekkor $b (q_1 - q_2) = r_2 - r_1$.  
   Így $q_1 \ne q_2$ miatt $|b (q_1 - q_2)| = |b| \cdot |q_1 - q_2| \ge |b| \cdot 1 =|b|$,  
   míg $0 \le r_1, r_2 < b$ miatt $|r_2 - r_1| < |b|$,  
   így $|b (q_1 - q_2)| \ne |r_2 - r_1|$, ami ellentmondás.  
   Ezért $q_1 = q_2$ és $r_1 = r_2$.

## Osztási maradék (Definíció)

Legyenek $a$ és $b$ egész számok ($b \ne 0$).  
Legyen $a = b \cdot q + r$, ahol $q$ és $r$ egészek, $0 \le r < |b|$.  
Ekkor $a \bmod b = r$ az $a$ szám $b$-vel vett osztási maradéka.

### Megjegyzés

- $q = \lfloor a/b \rfloor$, ha $b > 0$, és $q = \lceil a/b \rceil$, ha $b < 0$.

### Példák

- $123 \bmod 10 = 3$, $123 \bmod 100 = 23$, $123 \bmod 1000 = 123$
- $123 \bmod -10 = 3$, $\dots$
- $-123 \bmod 10 = 7$, $-123 \bmod 100 = 77$, $-123 \bmod 1000 = 877$
- $-123 \bmod -10 = 7$, $\dots$

### Szöveges példák

1. Ha most $9$ óra van, hány óra lesz $123$ óra múlva?  
   Osszuk el maradékosan $123$-at $24$-gyel: $123 = 24 \cdot 5 + 3$.  
   Tehát $9 + 3 = 12 \implies$ déli $12$ óra lesz!
2. Ha most $9$ óra van, hány óra lesz $116$ óra múlva?  
   Osszuk el maradékosan $116$-ot $24$-gyel: $116 = 24 \cdot 4 + 20$.  
   Tehát $9 + 20 = 29$.  
   Újabb redukció: $29 = 24 \cdot 1 + 5 \implies$ hajnali 5 óra lesz!
3. Tegyük fel, hogy ma 2014. november 11-e (kedd) van.  
   Milyen napra fog esni jövőre november 11-e?  
   Milyen napra esett három éve november 15-e?
   - hétfő $\rightarrow 0$, kedd $\rightarrow 1$, $\dots$, vasárnap $\rightarrow 6$
   - Osszuk el maradékosan $365$-öt $7$-tel: $365 = 7 \cdot 52 + 1$.  
   kedd $+ 1$ nap $\lrarr 1 + 1 = 2 \lrarr$ szerda
   - Osszuk el maradékosan $-(365+365+366)$-ot (2012.
szökőév) $7$-tel: $-1096 = 7 \cdot (-157) + 3$.  
szombat $+ 3$ nap $\lrarr 5 + 3 = 8$
$\implies$ $1 \lrarr kedd$

---

Következő: [Számrendszerek](./szamrendszerek)
