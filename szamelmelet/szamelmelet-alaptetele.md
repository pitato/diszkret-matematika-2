---
title: "Számelmélet alaptétele"
date: "2020-11-11"
tags:
  - Diszkrét Matematika 2
  - Számelmélet
---

## Számelmélet alaptétele (Tétel)

Minden $0$-tól és egységektől különböző egész szám sorrendtől és asszociáltaktól eltekintve egyértelműen felírható prímszámok szorzataként.

### Bizonyítás

Csak nemnegatív számokra.  
**Létezés**: Indukcióval: $n = 2$ esetén igaz (prím).  
Általában ha $n$ prím, akkor készen vagyunk, ha nem, akkor szorzatra bomlik nemtriviális módon.  
A tényezők már felbonthatók indukció alapján.  
**Egyértelműség**: Indukcióval: $n = 2$ esetén igaz (felbonthatatlan).  
Általában, ha $n$ felbonthatatlan és így a felbontás egyértelmű.(Miért?)  
Tfh. $n$ felbontható és minden $n$-nél kisebb számnak lényegében egyértelmű a prímek szorzataként való felírása.  
Legyen $n = p_1 p_2 \cdots p_k = q_1 q_2 \cdots q_l$ az $n$ két felbontása.  
Az általánosság megszorítása nélkül feltehető, hogy $p_1, p_2, \dots, p_k$ és $q_1, q_2, \dots, q_l$ mind pozitívak.  
Ekkor $p_1 p_2 \cdots p_k = q_1 q_2 \cdots q_l$ és $p_1$ osztja a bal oldalt, ezért osztja a jobb oldalt, így a prímtulajdonság miatt osztja annak valamelyik tényezőjét; feltehető $p_1 \mid q_1$.  
Mivel $q_1$ felbonthatatlan (hiszen prím), ezért $p_1 = q_1$.  
Egyszerűsítve: $n' = p_2 \cdots p_k = q_2 \cdots q_l$.  
Indukció alapján ez már egyértelmű.

## Kanonikus alak (Definíció)

Egy $0$-tól és egységektől különböző $n$ egész szám kanonikus alakja:

$n =\pm p^{\alpha_1}_1 p^{\alpha_2}_2 \cdots p^{\alpha_l}_l = \pm \displaystyle\prod_{i = 1}^l p^{\alpha_i}_i$  
ahol $p_1, p_2, \dots, p_l$ különböző pozitív prímek, $\alpha_1, \alpha_2, \dots, α_l$ pozitív egészek.

### Következmények

Legyenek $n, m > 1$ pozitív egészek:  
$n =\pm p^{\alpha_1}_1 p^{\alpha_2}_2 \cdots p^{\alpha_l}_l$  
$m =\pm p^{\beta_1}_1 p^{\beta_2}_2 \cdots p^{\beta_l}_l$  
(ahol $\alpha_i, \beta_i \ge 0$ nemnegatív egészek)  
Ekkor

1. $(n, m) = p_1^{\min(\alpha_1, \beta_1)} p_2^{\min(\alpha_2, \beta_2)} \cdots p^{\min(\alpha_l, \beta_l)}_l$
2. $[n, m] = p_1^{\max(\alpha_1, \beta_1)} p_2^{\max(\alpha_2, \beta_2)} \cdots p^{\max(\alpha_l, \beta_l)}_l$
3. $(n, m)\cdot[n, m] = n \cdot m$
4. $(n, m) = 1 \implies [n, m] = n \cdot m$.

## $\tau (n)$ (Definíció)

Egy $n > 0$ egész esetén legyen $\tau (n)$ az $n$ pozitív osztóinak száma.

### Példa

$\tau (6) = 4$, osztók: $1, 2, 3, 6 \quad \tau (96) = 12$, osztók: $1, 2, 3, 4, 6, 8, \dots$

## Osztók száma a kanonikus alakból (Tétel)

Legyen $n > 1$ egész, $n =\pm p^{\alpha_1}_1 p^{\alpha_2}_2 \cdots p^{α_l}_l$ kanonikus alakkal. Ekkor  
$\tau (n) = (\alpha_1 + 1) \cdot (\alpha_2 + 1)\cdots(\alpha_l + 1)$

### Bizonyítás

$n$ lehetséges osztóit úgy kapjuk, hogy a $d =\pm p^{\beta_1}_1 p^{\beta_2}_2 \cdots p^{\beta_l}_l$ kifejezésben az összes $\beta_i$ kitevő végigfut a $\{ 0, 1, \dots, \alpha_i\}$ halmazon.  
Így ez a kitevő
$\alpha_i + 1$-féleképpen választható.

### Példák

- $\tau (2 \cdot 3) = (1 + 1) \cdot (1 + 1) = 4$
- $\tau (25 \cdot 3) = (5 + 1) \cdot (1 + 1) = 12$

---

Következő: [Kongruenciák](./kongruenciak)
