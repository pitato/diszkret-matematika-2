---
title: "RSA"
date: "2020-11-11"
tags:
  - Diszkrét Matematika 2
  - Számelmélet
---

## Euklidesz (Tétel)

Végtelen sok prím van.

### Bizonyítás

Indirekt tfh. csak véges sok prím van. Legyenek ezek $p_1, \dots,p_k$  
Tekintsük az $n = p_1 \cdots p_{k + 1}$ számot.  
Ez nem osztható egyetlen $p_1, \dots, p_k$ prímmel sem (Miért?), így $n$ prímtényezős felbontásában kell szerepelnie egy újabb prímszámnak.

## Dirichlet (Tétel - nem bizonyított)

Ha $a, d$ egész számok, $d > 0,\ (a, d) = 1$, akkor végtelen sok $a + kd$ alakú ($k \in \N$) prím van.

## pi (x)

Tetszőleges $x \in \N^+$ esetén $\pi (x)$ jelöli az $x$-nél nem nagyobb, pozitív prímek számát.

## Prímszámtétel (Tétel - nem bizonyított)

$\pi (x) ∼ \frac{x} {\ln x}$ (Sok prím van!)

Prímek száma:

| $x$     | $\pi (x)$ | $\frac{x}{\ln x}$ |
| --------- | ----------- | ------------------- |
| $10$    | $4$       | $4,33$            |
| $100$   | $25$      | $21,71$           |
| $1000$  | $168$     | $144,76$          |
| $10000$ | $1229$    | $1085,73$         |

## Eratoszthenész szitája

Keressük meg egy adott $n$-ig az összes prímet.  
Soroljuk fel $2$-től $n$-ig az egész számokat.  
Ekkor $2$ prím. A $2$ (valódi) többszörösei nem prímek, ezeket húzzuk ki.
A következő (ki nem húzott) szám a $3$, ez szintén prím. A $3$ (valódi) többszörösei nem prímek, ezeket húzzuk ki$\dots$  
Ismételjük az eljárást $\sqrt{n}$-ig. A ki nem húzott számok mind prímek.

## RSA

Ron Rivest, Adi Shamir és Leonard Adleman 1977-ben a következő
eljárást javasolták:

Kulcsgenerálás: Legyen $p, q$ két (nagy, 1024 bites) prím, $n = p \cdot q$  
Legyen $e \in \{1 , \dots, \varphi (n)\}$ olyan, hogy $(e,\varphi (n)) = 1$  
Legyen $d$ az $ex \equiv 1\ (\bmod \varphi (n))$ kongruencia megoldása.  
Kulcsok:

- nyilvános kulcs $(n, e)$
- titkos kulcs $d$

Titkosítás: Adott $0 \ge m < n$ üzenet titkosítása:  
$c = m^e \bmod n$  
Kititkosítás: Adott $0 \ge c < n$ titkosított üzenet kititkosítása:  
$m = c^d \bmod n$

Algoritmus helyessége:  
$c^d = (m^e) d = m^{e \cdot d} = m^{k \cdot \varphi (n) + 1}\overset{\text{E-F}}{\equiv} m\ (\bmod n)$

Valóságban az $m$ üzenet egy titkos kulcs további titkosításhoz.  
Az eljárás biztonsága azon múlik, hogy nem tudjuk hatékonyan faktorizálni az $n = p \cdot q$ szorzatot.

### Feladat

Találjuk meg a következő szám osztóit.  
RSA-100 =  
1522605027922533360535618378132637429718068114961380688657908494580122963258952897654000350692006139  
RSA-2048 =  
25195908475657893494027183240048398571429282126204032027777137836043662020707595556264018525880784406918290641249515082189298559149176184502808489120072844992687392807287776735971418347270261896375014971824691165077613379859095700097330459748808428401797429100642458691817195118746121515172654632282216869987549182422433637259085141865462043576798423387184774447920739934236584823824281198163815010674810451660377306056201619676256133844143603833904414952634432190114657544454178424020924616515723350778707749817125772467962926386356373289912154831438167899885040445364023527381951378636564391212010397122822120720357

RSA-2048 faktorizálása:  
Próbaosztás (Eratoszthenész szitája): $n$ szám esetén $∼\sqrt{n}$ osztást kell végezni:

RSA-2048 $n ∼ 2^{2048}, \sqrt{n} ∼ 2^{1024}$ próbaosztás.

Ha 1 másodperc alatt $∼ 10^9 \approx 2^{30}$ osztás $\implies 2^{1024} / 2^{30} = 2^{994}$ másodperc kell a faktorizáláshoz.  
$2^{994}$ másodperc $\approx 2^{969}$ év.  
Ugyanezt 2 db géppel: $2^{968}$ év.

Ugyanezt a legjobb (ismert) algoritmussal:
$2500000000000000000000000000000$ év ($= 2,5 \cdot 10^{30}$)

Univerzum életkora: $1,38 \cdot 10^{10}$ év.

### Példa

- Kulcsgenerálás:  
  Legyen $p = 61, q = 53$ és $n = 61 \cdot 53 = 3233, \varphi (3233) = 3120$  
  Legyen $e = 17$. Bővített euklideszi algoritmussal: $d = 2753$  
  Nyilvános kulcs: $( n = 3233, e = 17)$  
  Titkos kulcs: $d = 2753$
- Titkosítás: Legyen $m = 65$  
  $c = 2790 \equiv 65^{17}\ (\bmod 3233)$
- Kititkosítás: Ha $c = 2790$:  
  $2790^{2753} \equiv 65\ (\bmod 3233)$

Digitális aláírást is lehet generálni: $e$ és $d$ felcserélésével:  
(Ekkor külön $n', e', d'$ kell a titkosításhoz!)

Aláírás Legyen $s = m^d mod n$, ekkor az aláírt üzenet: $(m, s)$  
Ellenőrzés $m \overset{?}{\equiv} s^e\ (\bmod n)$.

---

Következő: [Algebra - Műveletek](../algebra/muveletek)
