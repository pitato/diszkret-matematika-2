---
title: "Lineáris kongruenciák"
date: "2020-11-11"
tags:
  - Diszkrét Matematika 2
  - Számelmélet
---

Oldjuk meg a $2x \equiv 5\ (\bmod 7)$ kongruenciát!  
Ha $x$ egy megoldás és $x \equiv y\ (\bmod 7)$, akkor $y$ szintén megoldás.  
Keressük a megoldást a $\{0, 1, \dots, 6\}$ halmazból!  
$x = 0 \implies 2 x = 0 \not\equiv 5\ (\bmod 7)$  
$x = 1 \implies 2 x = 2 \not\equiv 5\ (\bmod 7)$  
$x = 2 \implies 2 x = 4 \not\equiv 5\ (\bmod 7)$  
$x = 3 \implies 2 x = 6 \not\equiv 5\ (\bmod 7)$  
$x = 4 \implies 2 x = 8 \equiv 1\ \not\equiv 5\ (\bmod 7)$  
$x = 5 \implies 2 x = 10 \equiv 3\ \not\equiv 5\ (\bmod 7)$  
$x = 6 \implies 2 x = 12 \equiv 5\ (\bmod 7)$  
A kongruencia megoldása: $\{6 + 7l : l \in \Z\}$

Van-e jobb módszer?  
Oldjuk meg a $23x \equiv 4\ (\bmod 211)$ kongruenciát! Kell-e 211 próbálkozás?

## Lineáris kongruenciák megoldása (Tétel)

Legyenek $a, b, m$ egész számok, $m > 1$.  
Ekkor az $ax \equiv b (\bmod m)$ kongruencia pontosan akkor oldható meg, ha $(a, m) \mid b$  
Ez esetben pontosan $(a, m)$ darab páronként inkongruens megoldás van $\bmod m$.

### Bizonyítás

$ax \equiv b\ (\bmod m) \iff m \mid b - ax \iff ax + my = b$ valamely y egészre.  
Tehát $ax \equiv b\ (\bmod m)$ megoldásai pontosan $ax + my = b$ "$x$-beli" megoldásai lesznek.  
Mivel $ax + my = b$ pontosan akkor oldható meg, ha $(a, m) \mid b$, így $ax \equiv b$ megoldhatáságának is ugyanez a feltétele.  
A kétváltozós, lineáris, diofantikus egyenletekről tanultak alapján tehát a kongruencia megoldásai az $x_t = x_0 + \frac{m}{(a, m)} \cdot t$ alakú számok lesznek, ahol $t \in \Z$ és $x_0$ egy tetszőleges megoldás.  
Tekintsük a következő $(a, m)$ db megoldást:  
$x_k = x_0 + k \frac{m}{(a, m)} : k = 0, 1, \dots, (a, m)- 1$  
Ezek páronként inkongruensek $\bmod m$ (Miért?), és bármely $x$ megoldás esetén van köztük $x$-szel kongruens $\bmod m$ (Miért?).

1. $ax \equiv b\ (\bmod m) \iff \exists y \in \Z: ax + my = b$
2. Pontosan akkor van megoldás, ha $(a, m) \mid b$
3. Oldjuk meg az $ax' + my' = (a, m)$ egyenletet (bővített euklideszi algoritmus)!
4. Megoldások: $x_k = \frac{b}{(a, m)} x'+ k \frac{m}{(a, m)} : k = 0, 1, \dots, (a, m) - 1$

### Példa

Oldjuk meg a $23 x \equiv 4\ (\bmod 211)$ kongruenciát!

| $i$  | $r_i$ | $q_i$ | $x_i'$ |
| ------ | ------- | ------- | -------- |
| $-1$ | $23$  | $-$   | $1$    |
| $0$  | $211$ | $-$   | $0$    |
| $1$  | $23$  | $0$   | $1$    |
| $2$  | $4 $  | $9$   | $-9$   |
| $3$  | $3 $  | $5$   | $46$   |
| $4$  | $1 $  | $1$   | $-55$  |
| $5$  | $0 $  | $3$   | $-$    |

### Algoritmus

- $r_{i - 2} = r_{i - 1} q_i + r_i$
- $x_{-1}' = 1,\ x_0' = 0$
- $x_i' = x_{i - 2}' - q_i x_{i - 1}'$

Lnko: $(23, 211) = 1 \mid 4 \implies$  
Egy megoldás: $x_0 = 4 (- 55)\equiv 202\ (\bmod 211)$

Összes megoldás: $\{202 + 211l : l \in \Z\}$  
(Ell.: ezek megoldások: $23 \cdot (202 + 211l)- 4 = 4642 + 211l = (22 + l) \cdot 211$)

### Példa

Oldjuk meg a $10x \equiv 8\ (\bmod 22)$ kongruenciát!

| $i$  | $ri$ | $q_i$ | $x_i'$ |
| ------ | ------ | ------- | -------- |
| $-1$ | $10$ | $-$   | $1$    |
| $0$  | $22$ | $-$   | $0$    |
| $1$  | $10$ | $0$   | $1$    |
| $2$  | $2$  | $2$   | $-2$   |
| $3$  | $0$  | $5$   | $-$    |

Lnko: $(10, 22) = 2 \mid 8 \implies$  
Két inkongruens megoldás:

- $x_0 = 4 (- 2) \equiv 14\ (\bmod 22)$
- $x_1 = 4 (- 2) + 1 \cdot \frac{22}{2} \equiv 14 + 11 \equiv 3\ (\bmod 22)$

Összes megoldás: $\{14 + 22l : l \in \Z\} \bigcup \{3 + 22l : l \in \Z\}$

Ezek megoldások:

- $x_0 = 14 : 10 \cdot 14 - 8 = 132 = 6 \cdot 22$
- $x_1 = 3 : 10 \cdot 3 - 8 = 22 = 1 \cdot 22$

---

Következő: [Szimultán kongruenciák](./szimultan-kongruenciak)
