---
title: "Maradékrendszerek"
date: "2020-11-11"
tags:
  - Diszkrét Matematika 2
  - Számelmélet
---

## Teljes maradékrendszer (Definíció)

Egy rögzített m modulus esetén egy olyan számhalmazt, amely minden modulo m maradékosztályból pontosan egy számot tartalmaz, teljes maradékrendszernek nevezzük modulo m.

### Példa

$\{ 33, -5, 11, -11, -8 \}$ teljes maradékrendszer modulo $5$.

Gyakori választás teljes maradékrendszerekre

- Lehetséges $\bmod m$ maradékok: $\{0, 1, \dots, m - 1\}$
- "Legkisebb abszolút értékű maradékok":
  - $\{ 0, \pm 1, \dots, \pm \frac{m - 1}{2} \}$, ha $2 \nmid m$
  - $\{ 0, \pm 1, \dots, \pm \frac{m - 2}{2}, \frac{m}{2}\}$, ha $2 \mid m$

### Megjegyzés

Ha egy maradékosztály valamely eleme relatív prím a modulushoz, akkor az összes eleme az: $(a + lm, m) = (a, m) = 1.$  
Ezeket a maradékosztályokat redukált maradékosztályoknak nevezzük.

## Redukált maradékrendszer (Definíció)

Rögzített m modulus esetén egy olyan számhalmazt, amely pontosan egy számot tartalmaz minden modulo m redukált maradékosztályból, redukált maradékrendszernek nevezünk modulo m.

### Példák

- $\{ 1, 2, 3, 4 \}$ redukált maradékrendszer modulo $5$
- $\{ 1, -1 \}$ redukált maradékrendszer modulo $3$
- $\{ 1, 19 , 29 , 7 \}$ redukált maradékrendszer modulo $8$
- $\{ 0, 1 , 2 , 3 , 4 \}$ nem redukált maradékrendszer modulo $5$

---

Következő: [Euler-féle phi függvény](./euler-fele-phi-fuggveny)
