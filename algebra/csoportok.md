---
title: "Csoportok"
date: "2020-11-14"
tags:
  - Diszkrét Matematika 2
  - Algebra
---

## Elem inverze (Definíció)

Legyen $(G; ∗)$ egy egységelemes félcsoport $e$ egységelemmel.  
A $g \in G$ elem inverze az a $g^{-1} \in G$ elem, melyre $g ∗ g^{-1} = g^{-1} ∗ g = e$

Egy elemnek nem feltétlenül létezik inverze, de ha létezik, akkor egyértelmű.  
(Miért is? Kell hozzá a művelet asszociativitása!)

## Csoport (Definíció)

Ha a $(G; ∗)$ egy egységelemes félcsoportban minden $g \in G$ elemnek létezik inverze, akkor $(G; ∗)$ csoport.

## Abel-csoport (Definíció)

Ha a $(G; ∗)$ csoportban a $∗$ csoportművelet kommutatív, akkor $(G; ∗)$ Abel-csoport.

### Példák

- $(\Z; +)$, $(\text{Q}; +)$, $(\R; +)$ és $(\cnums; +)$ a $0$ egységelemmel Abel-csoportok.
- $(\text{Q}^∗; \cdot)$, $(\R^∗; \cdot)$ és $(\cnums^∗; \cdot)$ az $1$ egységelemmel Abel-csoportok, ahol $\text{Q}^∗ = \text{Q} \setminus \{ 0 \}, \R^∗ = \R \setminus \{ 0 \},\ \cnums^∗ = \cnums \setminus \{ 0 \}$
- $\{M \in \cnums^{k \times k} : \det M \ne 0\}$ a mátrixszorzással, és az egységmátrixszal mint egységelemmel, csoport (de $k > 1$ esetén nem Abel-csoport).
- $X \rightarrow X$ bijektív függvények a kompozícióval, az $id_x : x \rightarrow x$ identikus leképzéssel mint egységelemmel csoport, de nem Abel-csoport.

---

Következő: [Gyűrűk](./gyuruk)
