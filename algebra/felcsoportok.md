---
title: "Félcsoportok"
date: "2020-11-14"
tags:
  - Diszkrét Matematika 2
  - Algebra
---

## Félcsoport (Definíció)

A $(G; ∗)$ grupoid félcsoport, ha $∗$ asszociatív $G$-n, azaz, ha:  
$\forall a, b, c \in G: (a ∗ b) ∗ c = a ∗ (b ∗ c)$

## Egységelem, semleges elem, monoid (Definíció)

Ha a $(G; ∗)$ félcsoportban létezik olyan $s \in G$ elem, amelyre  
$\forall g \in G: s ∗ g = g ∗ s = g$, akkor az $s$ elemet semleges elemnek (más néven egységelemnek) nevezzük.  
Ekkor $(G; ∗)$ semleges elemes félcsoport, egységelemes félcsoport, más néven monoid.

### Példák

- $\N$ az $+$ művelettel egységelemes félcsoport a $0$ egységelemmel.
- Q a $\cdot$ művelettel egységelemes félcsoport az $1$ egységelemmel.
- $C^{k \times k}$ a mátrixszorzással egységelemes félcsoport az egységmátrixszal mint egységelemmel.

---

Következő: [Csoportok](./csoportok)
