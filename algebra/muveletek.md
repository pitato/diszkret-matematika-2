---
title: "Műveletek"
date: "2020-11-14"
tags:
  - Diszkrét Matematika 2
  - Algebra
---

## Művelet (Definíció)

Egy $X$ halmazon értelmezett ($r$-változós, "$r$-ér") művelet alatt egy $* : X^r \rightarrow X$ függvényt értünk.  
Egy $X$ halmazon értelmezett binér (kétváltozós) művelet egy $* : X \times X \rightarrow X$ függvény. Gyakran $*(x,y)$ helyett $x * y$-t írunk.  
Egy $X$ halmazon értelmezett unér (egyváltozós) művelet egy $* : X \rightarrow X$ függvény.

### Példa

- $\cnums$ halmazon az $+$ és $\cdot$ binér műveletek.
- $\cnums$ halmazon az $\div$ (osztás) nem művelet, mert $\text{dmn}(\div) \ne \cnums \times \cnums$
- $\cnums^* = \cnums \setminus \{0\}$ halmazon az $\div$ binér művelet.
- $\cnums$ halmazon a $0$ illetve $1$ konstans kijelölése nullér müvelet.
- $\R^n\ (n > 1)$ vektortéren a vektorok skaláris szorzása nem művelet, mert $\text{rng}(\lang , \rang) = \R \ne \R^n$, tehát a szorzás eredménye nem vektor.
- $\R^n$ vektortéren egy adott $\lambda \in \R$ skalárral való szorzás unér művelet.

---

Következő: [Algebrai struktúták](./algebrai-strukturak)
