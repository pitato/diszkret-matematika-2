---
title: "Algebrai struktúrák"
date: "2020-11-14"
tags:
  - Diszkrét Matematika 2
  - Algebra
---

## Algebrai struktúra (Definíció)

A $(H;M)$ pár algebrai struktúra, ha $H$ egy halmaz, $M$ pedig $H$-n értelmezett műveletek halmaza.

A $(H; \{∗, +, \circ\})$ jelölés helyett a $(H; ∗, +, \circ)$ jelölést is használhatjuk.

### Tanult típusai

- Grupoid
- Félcsoport
- Monoid
- Csoport
- Abel-csoport
- Gyűrű
  - egységelemes
  - kommutatív
  - nullosztómentes
  - mod m maradékosztályok gyűrűje
- Integritási tartomány
- Ferdetest
- Test

## Grupoid (Definíció)

Ha az $M$ művelethalmaz egyetlen műveletet tartalmaz, és az egy binér művelet, akkor $(H; M)$ struktúrát grupoidnak nevezzük.

### Példák

- $(\N; +)$ algebrai struktúra, mert természetes számok összege természetes szám, és grupoid is.
- $(\N; -)$ nem algebrai struktúra, mert például $0 - 1 = -1 \notin \N$
- $(\Z; + , \cdot)$ algebrai struktúra, mert egész számok összege és szorzata egész szám, de nem grupoid, mert két művelet van.
- $(\cnums; +, \cdot)$ algebrai struktúra, mert komplex számok összege és szorzata komplex szám, de nem grupoid, mert két művelet van.

---

Következő: [Félcsoportok](./felcsoportok)
