---
title: "Maradékosztályok gyűrűje"
date: "2020-11-14"
tags:
  - Diszkrét Matematika 2
  - Algebra
---

## Maradékosztályok halmaza (Definíció)

Tetszőleges $m$ egész esetén $\Z_m$ jelöli a $\bmod m$ maradékosztályok halmazát.

A $\bmod m$ maradékosztályok között (azaz $\Z_m$ elemein) természetes módon műveleteket definiálhatunk:

## Maradékosztályok összeadása és szorzása (Definíció)

Rögzített $m$ modulus, és $a, b$ egészek esetén legyen:  
$\overline{a} + \overline{b} \overset{def}{=} \overline{a + b};\ \overline{a} \cdot \overline{b} \overset{def}{=} \overline{a \cdot b}$

## A műveletek a maradékosztályokon jól definiáltak (Állítás)

Ez értelmes definíció, azaz, ha $\overline{a} = \overline{a^∗},\ b = \overline{b^∗}$,  
akkor $\overline{a} + \overline{b} = \overline{a^∗} + \overline{b^∗}$, illetve $\overline{a} \cdot \overline{b} = \overline{a^∗} \cdot \overline{b^∗}$

### Bizonyítás

Mivel $\overline{a} = \overline{a^∗}, \overline{b} = \overline{b^∗} \implies a \equiv a^∗\ (\bmod m),\ b \equiv b^∗\ (\bmod m)$  
$\implies a + b \equiv a^∗ + b^∗\ (\bmod m) \implies a + b = a^∗ + b^∗ \implies a + b = a^∗ + b^∗$  
Szorzás hasonlóan.

### Példák

- $\Z_3 = \{ \overline{0}, \overline{1}, \overline{2} \}$

| $+$            | $\overline{0}$ | $\overline{1}$ | $\overline{2}$ | $\cdot$        | $\overline{0}$ | $\overline{1}$ | $\overline{2}$ |
| -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- |
| $\overline{0}$ | $\overline{0}$ | $\overline{1}$ | $\overline{2}$ | $\mid$         | $\overline{0}$ | $\overline{0}$ | $\overline{0}$ |
| $\overline{1}$ | $\overline{1}$ | $\overline{2}$ | $\overline{0}$ | $\mid$         | $\overline{0}$ | $\overline{1}$ | $\overline{2}$ |
| $\overline{2}$ | $\overline{2}$ | $\overline{0}$ | $\overline{1}$ | $\mid$         | $\overline{0}$ | $\overline{2}$ | $\overline{1}$ |

- $\Z_4 = \{ \overline{0}, \overline{1}, \overline{2}, \overline{3} \}$

| $+$            | $\overline{0}$ | $\overline{1}$ | $\overline{2}$ | $\overline{3}$ | $\cdot$        | $\overline{0}$ | $\overline{1}$ | $\overline{2}$ | $\overline{3}$ |
| -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- | -------------- |
| $\overline{0}$ | $\overline{0}$ | $\overline{1}$ | $\overline{2}$ | $\overline{3}$ | $\mid$         | $\overline{0}$ | $\overline{0}$ | $\overline{0}$ | $\overline{0}$ |
| $\overline{1}$ | $\overline{1}$ | $\overline{2}$ | $\overline{3}$ | $\overline{0}$ | $\mid$         | $\overline{0}$ | $\overline{1}$ | $\overline{2}$ | $\overline{3}$ |
| $\overline{2}$ | $\overline{2}$ | $\overline{3}$ | $\overline{0}$ | $\overline{1}$ | $\mid$         | $\overline{0}$ | $\overline{2}$ | $\overline{0}$ | $\overline{2}$ |
| $\overline{2}$ | $\overline{3}$ | $\overline{0}$ | $\overline{1}$ | $\overline{2}$ | $\mid$         | $\overline{0}$ | $\overline{3}$ | $\overline{2}$ | $\overline{1}$ |

## Maradékosztályok gyűrűje egységelemes és kommutatív (Tétel)

Tetszőleges $m$ egész esetén $(\Z_m, +, \cdot)$ - ahol $+$ és $\cdot$ a maradékosztályok összeadását, ill. szorzását jelölik - egységelemes, kommutatív gyűrű.

Könnyen belátható, hogy ha $m$ nem prím, akkor $\Z_m$ nem nullosztómentes (tehát test sem lehet).

## mod prím maradékosztály gyűrűje test (Tétel)

Tetszőleges $p$ prím esetén $(\Z_p, +, \cdot)$ test.

---

Következő: [Nullosztómentes gyűrűk karakterisztikája](./nullosztomentes-gyuruk-karakterisztikaja)
