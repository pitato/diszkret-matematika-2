---
title: "Testek, ferdetestek"
date: "2020-11-14"
tags:
  - Diszkrét Matematika 2
  - Algebra
---

## Ferdetest (Definíció)

Az $(R; \oplus, \otimes)$ egységelemes gyűrű ferdetest, ha $(R \setminus \{ 0 \}; \otimes)$ csoport.

## Test (Definíció)

A kommutatív ferdetestet (azaz amiben nemcsak az összeadás, hanem a
szorzás is kommutatív) testnek nevezzük.

### Példa

Q, $\R$, $\cnums$ a szokásos műveletekkel testek.

## Test nullosztómentessége (Állítás)

Test nullosztómentes.

### Bizonyítás

Legyen $(F; \oplus, \otimes)$ test $0 \in F$ nullelemmel.  
Ekkor a definíció szerint $(F \setminus \{ 0 \}; \otimes)$ csoport, azaz $F \setminus \{ 0 \}$ zárt a $\otimes$ műveletre, ahonnan az állítás következik.

---

Következő: [Maradékosztályok gyűrűje](./maradekosztalyok-gyuruje)
