---
title: "Gyűrűk"
date: "2020-11-14"
tags:
  - Diszkrét Matematika 2
  - Algebra
---

## Disztributivitás (Definíció)

Legyen $(R; \oplus, \otimes)$ algebrai struktúra, ahol $\oplus$ és $\otimes$ binér műveletek.  
Azt mondjuk, hogy teljesül a $\otimes$-nak a $\oplus$-ra vonatkozó bal oldali disztributivitása, illetve jobb oldali disztributivitása, ha  
$\forall k, l, m \in \R$-re: $k \otimes (l \oplus m) = (k \otimes l) \oplus (k \otimes m)$, illetve  
$\forall k, l, m \in \R$-re: $(l \oplus m) \otimes k = (l \otimes k) \oplus (m \otimes k)$

### Példa

$(\Z; +, \cdot)$ esetén teljesül a szorzás összeadásra vonatkozó mindkét oldali disztributivitása.

## Szokásos elnevezések, bevett jelölések

$(R; \oplus, \otimes)$ két binér műveletes algebrai struktúra esetén szokás az $\oplus$ műveletet "összeadásnak" és a $\otimes$ műveletet "szorzásnak" nevezni (ha nem okoz félreértést).  
Az $\oplus$-ra vonatkozó semleges elemet ekkor nullelemnek, a $\otimes$-ra vonatkozó semleges elemet egységelemnek nevezzük.  
A nullelem szokásos jelölése $0$, az egységelemé $1$, esetleg $e$.

## Gyűrű (Definíció)

Az $(R; \oplus, \otimes)$ két binér műveletes algebrai struktúra gyűrű, ha

- $(R; \oplus)$ Abel-csoport (kommutatív csoport a $0$ egységelemmel);
- $(R; \otimes)$ félcsoport;
- teljesül a $\otimes$-nak a $\oplus$-ra vonatkozó mindkét oldali disztributivitása.

### Megjegyzés

$(R; \oplus, \otimes)$ gyűrű nulleleme $0$, tehát $(R;\oplus)$ Abel-csoport egységeleme $0$

### Példa

$(\Z, +, \cdot)$ gyűrű.

## Nullelemmel való szorzás gyűrűben (Állítás)

Legyen $(R; \oplus, \otimes)$ gyűrű $0 \in \R$ nullelemmel.  
Ekkor $\forall r \in \R$ esetén $0 \otimes r = r \otimes 0 = 0$

### Bizonyítás

$0 \otimes r = (0 \oplus 0) \otimes r = (0 \otimes r) \oplus (0 \otimes r) \implies 0 = 0 \otimes r$  
A másik állítás bizonyítása ugyanígy.

## Egységelemes gyűrű (Definíció)

Az $(R; \oplus, \otimes)$ gyűrű egységelemes gyűrű, ha $\R$-en a $\otimes$ műveletre nézve is van egységelem: $1$ vagy $e$  
Azaz ha $(R; \otimes)$ egységelemes félcsoport.

## Kommutatív gyűrű (Definíció)

Az $(R; \oplus, \otimes)$ gyűrű kommutatív gyűrű, ha a $\otimes$ művelet is kommutatív.  
Azaz ha $(R; \otimes)$ kommutatív félcsoport.

### Példa

- $(\Z; +, \cdot)$ egységelemes kommutatív gyűrű.
- $(2 \Z; +, \cdot)$ kommutatív gyűrű, de nem egységelemes.
- $\text{Q}, \R, \cnums$ a szokásos műveletekkel egységelemes kommutatív gyűrűk.
- $(Z^{k \times k}, +, \cdot)$, $(\text{Q}^{k \times k}, +, \cdot)$, $(\R^{k \times k}, +, \cdot)$ és $(\cnums^{k \times k}, +, \cdot)$ a szokásos mátrixösszeadással és mátrixszorzással egységelemes gyűrű, de nem kommutatív, ha $k > 1$
- $(R^3; +, \times)$ a $3$-dimenziós Euklideszi vektortér a vektoriális szorzással NEM gyűrű mert $\times$ nem asszociatív, ezért $(R^3; \times)$ nem félcsoport.

## Nullosztómentes gyűrű (Definíció)

Ha egy legalább kételemű $(R, \oplus, \otimes)$ gyűrűben  
$\forall r, s \in \R, r \ne 0, s \ne 0$ esetén $r \otimes s \ne 0$, akkor $R$ nullosztómentes gyűrű.  
(Ilyenkor $r \otimes s = 0 \implies r = 0 \lor s = 0$)

### Példa

Nem nullosztómentes gyűrű  
$(R^{2 \times 2}; +, \cdot) : \begin{pmatrix} 0 & 0 \\ 0 & 1 \end{pmatrix} \cdot \begin{pmatrix} 1 & 0 \\ 0 & 0 \end{pmatrix} = \begin{pmatrix} 0 & 0 \\ 0 & 0 \end{pmatrix}$

## Integritási tartomány (Definíció)

A kommutatív, nullosztómentes gyűrűt integritási tartománynak nevezzük.

### Példa

$(\Z; +, \cdot)$

## Oszthatóság egységelemes integritási tartományban (Definíció)

Az $(R; \oplus, \otimes)$ egységelemes integritási tartományban az $a, b \in R$ elemekre azt mondjuk, hogy $a$ osztója $b$-nek, ha van olyan $c \in R$, amire $b = a \otimes c$  
Jelölése: $a \mid b$

## Egységek egységelemes integritási tartományban (Definíció)

Egy egységelemes integritási taromány egy olyan elemét, amely az integritási tartomány minden elemének osztója, egységnek nevezünk.

Ne keverjük az egységelem és az egység fogalmát!  
Az egységelem mindig egység is, de nem az egységelemen kívül lehetnek más egységek is.  
Egységelemből csak egyetlen egy van (az $1$ jelöli), egységből esetleg (tipikusan) több is.

---

Következő: [Testek, ferdetestek](./testek-ferdetestek)
