---
title: "Nullosztómentes gyűrűk karakterisztikája"
date: "2020-11-14"
tags:
  - Diszkrét Matematika 2
  - Algebra
---

Jelölés: Tetszőleges $R$ gyűrű, $a \in R$ és $n \in \N^+$ esetén legyen  
$na = \underbrace{a + a + \dots + a}_{n}$  
($n$ tagú összeg, melynek minden tagja $a$)

## Elem additív rendje gyűrűben (Definíció)

Egy $R$ gyűrű egy $a$ elemének additív rendje a legkisebb olyan pozitív egész $n$, amelyre $na = 0$, ha ilyen $n$ létezik.  
Egyébként $a$ additív rendje végtelen.

## Elemek additív rendje nullosztómentes gyűrűben (Állítás)

Nullosztómentes gyűrűben a nem-nulla elemek additív rendje megegyezik, és vagy egy $p$ prímszám vagy végtelen.

## Nullosztómentes gyűrű karakterisztikája (Definíció)

Ha az előző állításban szereplő közös rend $p$, akkor azt mondjuk, hogy a gyűrű karakterisztikája $p$ (jelölése: $\text{char} (R) = p$),  
ha pedig ez a közös rend végtelen, akkor a gyűrű karakterisztikája $\text{char} (R) = 0$

### Példák

- $\text{char} (\Z) = \text{char} (\text{Q}) = \text{char} (\R) = \text{char} (\cnums) = 0$
- $\text{char} (\Z_p) = p$ ($p$ prím)

---

Következő: [Polinomok alapfogalmai](../polinomok/polinomok-alapfogalmai)
