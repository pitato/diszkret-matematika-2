---
title: "Polinomok algebrai deriváltjai"
date: "2020-11-15"
tags:
  - Diszkrét Matematika 2
  - Polinomok
---

## Polinomok algebrai deriváltja (Definíció)

Legyen $R$ gyűrű. Az  
$f(x) = f_n x^n + f_{n - 1} x^{n-1} + \dots + f_2 x^2 + f_1 x + f_0 \in R[x]\ (f_n \ne 0 )$  
polinom algebrai deriváltja az  
$f'(x) = n f_n x^{n - 1} + (n - 1) f_{n - 1} x^{n - 2} + \dots + 2 f_2 x + f_1 \in R[x]$  
polinom.

### Megjegyzés

Itt $k f_k = \underbrace{f_k + f_k + \dots + f_k}_{n\ \text{db}}$ (Ez akkor kell, ha $k \in \N^+$, de $k \notin R$)

## Gyűrűelemek szorzása pozitív természtes számmal (Állítás)

Legyen $R$ gyűrű, $a, b \in R$ és $n \in \N^+$  
Ekkor $(n a) b = n (a b) = a (n b)$

### Bizonyítás

$(\underbrace{a + a + \dots + a}_{n\ \text{db}}) b = (\underbrace{a b + a b + \dots + a b}_{n\ \text{db}}) = a (\underbrace{b + b + \dots + b}_{n\ \text{db}})$

## Az algebrai derivált tulajdonságai (Állítás)

Ha $R$ egységelemes integritási tartomány, akkor az $f \rightarrow f'$ algebrai deriválás rendelkezik a következő tulajdonságokkal:

1. konstans polinom deriváltja a nullpolinom
2. az x polinom deriváltja az egységelem
3. $(f + g)' = f' + g'$, ha $f, g \in R[x]$ (additivitás)
4. $(f g)' = f' g + f g'$, ha $f, g \in R[x]$ (szorzat differenciálási szabálya)

### Megjegyzés

Megfordítva, ha egy $R$ egységelemes integritási tartomány esetén egy $f \rightarrow f'$, $R[x]$-et önmagába képező leképzés rendelkezik az előző 4 tulajdonsággal, akkor az az algebrai deriválás.

## (x - c) az n-ediken algebrai deriváltja (Állítás)

Ha $R$ egységelemes integritási tartomány, $c \in R$ és $n \in \N^+$, akkor  
$((x - c)^n)' = n (x - c)^{n -1}$

### Bizonyítás

n szerinti teljes indukció:  
$n = 1$ esetén $(x - c)' = 1 = 1 \cdot (x - c)^0$  
Tfh. $n = k$-ra teljesül az állítás, vagyis $((x - c) k)' = k(x - c)^{k - 1}$  
Ekkor  
$((x - c)^{k + 1})'= ((x - c)^k (x - c))'= ((x - c)^k)'(x - c) + (x - c)^k (x - c)'=$  
$= k(x - c)^{k - 1} (x-c) + (x - c)^k \cdot 1 = (k + 1) (x - c) k$  
Ezzel az állítást beláttuk.

## Integritási tartománybeli nem-nulla elem szorzása a karakterisztikával osztható számmal (Állítás - nem bizonyított)

Ha $R$ integritási tartomány, $\text{char}(R) = p$ és $0 \ne r \in R$ akkor  
$n \cdot r = 0 \iff p \mid n$

## Polinom gyökének multiplicitása (Definíció)

Legyen $R$ egységelemes integritási tartomány, $0 \ne f \in R[x]$ és $n \in \N^+$  
Azt mondjuk, hogy $c\in R$ az $f$ egy $n$-szeres gyöke, ha  
$(x-c)^n \mid f$ de $(x - c)^{n + 1} \nmid f$  
Ekkor $c$ multiplicitása $n$

### Megjegyzés

A definíció azzal ekvivalens, hogy $f(x) = (x - c)^n g(x)$ ahol $c$ nem gyöke $g$-nek. (Miért?)

## Polinom gyökeinek multiplicitása és az algebrai derivált (Tétel)

Legyen $R$ egységelemes integritási tartomány, $f \in R[x],\ n \in \N^+$ és $c \in R$ az $f$ egy $n$-szeres gyöke.  
Ekkor $c$ az $f'$-nek legalább $(n - 1)$-szeres gyöke, és ha $\text{char}(R) \nmid n$, akkor pontosan $(n - 1)$-szeres gyöke.

### Bizonyítás

Ha $f(x) = (x - c)^n g(x)$, ahol $c$ nem gyöke $g$-nek, akkor  
$f'(x) = ((x - c)^n)' g(x) + (x - c)^n g'(x) =$  
$= n (x - c)^{n - 1} g(x) + (x - c)^n g'( x )=$  
$= (x - c)^{n - 1} (n g(x) + (x - c) g'(x))$  
Tehát $c$ tényleg legalább $(n - 1)$-szeres gyöke $f'$-nek, és akkor lesz $(n - 1)$-szeres gyöke, ha $c$ nem gyöke $n g(x) + (x - c) g'(x)$-nek, vagyis  
$0 \ne n g(c) + (c - c) g'(c) = n g(c) + 0 \cdot g'(c) = n g(c)$  
Ez pedig teljesül, ha $\text{char}(R) \nmid n$

### Példa

Legyen $f(x) = x^4 - x \in \Z_3[ x ]$.  
Ekkor $1$ $3$-szoros gyöke $f$-nek, mert  
$f(x) = x (x^3 - 1) \overset{\Z_3}{=} x (x^3 - 3 x^2 + 3x - 1) = x (x - 1)^3$  
$f'(x) = (x - 1)^3 + 3x (x - 1)^2 = (x - 1)^2 (4x - 1) \overset{\Z_3}{=}$  
$\overset{\Z_3}{=} (x - 1)^2 (x - 1) = (x - 1)^3$  
tehát $1$ $3$-szoros gyöke $f'$-nek is.

---

Következő: [Lagrange-interpoláció](./lagrange-interpolacio)
