---
title: "Polinomok felbonthatósága"
date: "2020-11-15"
tags:
  - Diszkrét Matematika 2
  - Polinomok
---

## Felbonthatatlan és felbontható polinomok (Definíció)

Legyen $R$ egységelemes integritási tartomány.  
Egy $0 \ne f \in R[x]$ nem egység polinomot pontosan akkor nevezünk felbonthatatlannak (irreducibilisnek), ha $\forall a, b \in R[x]$-re  
$f = a \cdot b \implies (a\ \text{egység} \lor b\ \text{egység})$

Ha a $0 \ne f \in R[x]$ polinom nem egység és nem felbonthatatlan, akkor felbonthatónak (reducibilisnek) nevezzük.

### Megjegyzés

Utóbbi azt jelenti, hogy $f$-nek van nemtriviális szorzat-előállítása (olyan, amiben egyik tényező sem egység).

A konstans nulla polinom és az egység polinomok se nem
felbonthatatlanok, se nem felbonthatók.

## Egységek test feletti polinomgyűrűben (Állítás)

Legyen $(F; +, \cdot)$ test.  
Ekkor $f \in F[x]$ pontosan akkor egység, ha $\deg(f) = 0$

### Bizonyítás

- $\impliedby$  
Ha $\deg(f) = 0$ akkor $f$ nem-nulla konstans polinom: $f(x) = f_0$  
Mivel $F$ test, ezért létezik $f_0^{-1} \in F$ amire $f_ 0 \cdot f_0^{-1} = 1$ ezért minden $g(x) \in F[x]$-re  
$g = f_ 0 \cdot f_0^{-1} \cdot g = f \cdot(f_0^{-1} \cdot g)$  
Tehát $f$ osztja $g$-t, így $f$ tényleg egység.
- $\implies$  
Ha $f$ egység, akkor létezik $g \in F[x]$ amire $f \cdot g = 1$ és így $\deg(f) + \deg(g) = deg(1) = 0$ (Miért?) ami csak $\deg(f) = \deg(g) = 0$ esetén lehetséges.

## Test feletti elsőfokú polinomok (Állítás)

Legyen $(F; +, \cdot)$ test, és $f \in F[x]$  
Ha $\deg(f) = 1$ akkor $f$-nek van gyöke.

### Bizonyítás

Ha $\deg(f) = 1$ akkor felírható $f(x) = f_1 x + f_0$ alakban, ahol $f_1 \ne 0$  
Azt szeretnénk, hogy létezzen $c \in F$ amire $f(c) = 0$ vagyis $f_1 c + f_0 = 0$  
Ekkor $f_1 c = -f_0$ (Miért?), és mivel létezik $f_1^{-1} \in F$ amire $f_1 \cdot f_1^{-1} = 1$ (Miért?)  
Ezért $c = -f_0 \cdot f_1^{-1} = -\frac{f_0}{f_1}$ gyök lesz.

### Megjegyzés

Ha $(R; +, \cdot)$ nem test, akkor egy $R$ fölötti elsőfokú polinomnak nem feltétlenül van gyöke, pl. $2x - 1 \in \Z[x]$ polinomnak nincs egész gyöke.  
(És emlékezzünk, hogy $R[x]$-beli polinomnak csak $R$-beli gyökeit
definiáltuk)

## Test feletti elsőfokú polinomok felbonthatatlansága (Állítás)

Legyen $(F; +, \cdot)$ test, és $f \in F[x]$  
Ha $\deg(f) = 1$ akkor $f$ felbonthatatlan.

### Bizonyítás

Legyen $f = g \cdot h$  
Ekkor $\deg(g) + \deg(h) = \deg(f) = 1$ (Miért?) miatt  
$\deg(g) = 0 \land \deg(h) = 1$ vagy $\deg(g) = 1 \land \deg(h) = 0$  
Előbbi esetben $g$, utóbbiban $h$ egység a korábbi állítás értelmében.

### Megjegyzés

Tehát nem igaz, hogy egy felbonthatatlan polinomnak nem lehet gyöke.

## Test feletti másod- és harmadfokú polinomok felbonthatósága (Állítás)

Legyen $(F; +, \cdot)$ test, és $f \in F[x]$  
Ha $2 \le \deg(f) \le 3$ akkor $f$ pontosan akkor felbontható, ha van gyöke.

### Bizonyítás

- $\impliedby$  
Ha $c$ gyöke $f$-nek, akkor az $f(x) = (x - c) g(x)$ egy nemtriviális felbontás. (Miért?)
- $\implies$  
Mivel $2 = 0 + 2 = 1 + 1$ illetve $3 = 0 + 3 = 1 + 2$ és más összegként nem állnak elő, ezért amennyiben $f$-nek van nemtriviális felbontása, akkor van elsőfokú osztója.  
A korábbi állítás alapján ennek van gyöke, és ez nyilván $f$ gyöke is lesz.

## Felbonthatatlan polinomok C felett (Tétel)

$f \in \cnums[x]$ pontosan akkor felbonthatatlan, ha $\deg(f) = 1$

### Bizonyítás

- $\impliedby$  
MivelCa szokásos műveletekkel test, ezért korábbi állítás alapján
teljesül.
- $\implies$  
Indirekt tfh. $\deg(f) \ne 1$  
Ha $\deg(f) < 1$ akkor $f = 0$ vagy $f$ egység, tehát nem felbonthatatlan, ellentmondásra jutottunk.  
$\deg(f) > 1$ esetén az algebra alaptétele értelmében van gyöke $f$-nek.  
A gyöktényezőt kiemelve az $f(x) = (x - c) g(x)$ alakot kapjuk, ahol $\deg(g) \ge 1$ (Miért?), vagyis egy nemtriviális szorzat-előállítást, így $f$ nem felbonthatatlan, ellentmondásra jutottunk.

Az algebra alaptételét itt használtuk, de nem bizonyítottuk!

## Felbonthatatlan polinomok R felett (Tétel)

$f \in \R[x]$ pontosan akkor felbonthatatlan, ha

- $\deg(f) = 1$ vagy
- $\deg(f) = 2$ és $f$-nek nincs (valós) gyöke.

### Bizonyítás

- $\impliedby$  
Ha $\deg(f) = 1$ akkor korábbi állítás (test fölötti elsőfokú polinom ...) alapján $f$ felbonthatatlan.  
Ha $\deg(f) = 2$ és $f$-nek nincs gyöke, akkor korábbi állítás (test fölötti másodfokú polinom ...) alapján $f$ felbonthatatlan.
- $\implies$  
Ha $f$ felbonthatatlan, akkor nem lehet $\deg(f) < 1$ (Miért?)  
Ha $f$ felbonthatatlan, és $deg(f) = 2$ akkor nem lehet gyöke. (Miért?)

De még nem vagyunk kész! Még nem láttuk, hogy ne lehetne kettőnél
magasabb fokú egyRfelett irreducibilis polinom ...

Tfh. $deg(f) \ge 3$  
Az algebra alaptétele értelmében $f$-nek mint $\cnums$ fölötti
polinomnak van $c \in \cnums$ gyöke.  
Ha $c\in R$ is teljesül, akkor a gyöktényező kiemelésével $f$ egy nemtriviális felbontását kapjuk (Miért?), ami ellentmondás.  
Legyen most $c \in \cnums \setminus \R$ gyöke $f$-nek, és tekintsük a  
$g(x) = (x - c)(x - \overline{c}) = x^2 - 2 \text{Re}(c) x + |c|^2 \in R[x]$ polinomot.  
$f$-et $g$-vel maradékosan osztva létezik $q, r \in R[x]$ hogy $f = q g + r$  
$r = 0$ mert $\deg(r) < 2$ és $r$-nek gyöke $c \in \cnums \setminus \R$  
Vagyis $f = q g$ ami egy nemtriviális felbontás, ez pedig ellentmondás.

### Megjegyzés

Ha $f \in R[x]$-nek $c \in \cnums$ gyöke, akkor $c$ is gyöke, hiszen
$$f(\overline{c}) = \sum^{\deg(f)}_{j = 0} f_j (\overline{c})^j = \sum^{\deg(f)}_{j = 0} \overline{f_j} \cdot \overline{c^j} = \sum^{\deg(f)}_{j = 0} \overline{f_j c^j} = \overline{\bigg(\sum^{\deg(f)}_{j = 0} f_j c^j\bigg)} = \overline{f(c)} = \overline{0} = 0$$

## Primitív polinom (Definíció)

$f \in \Z[x]$-et primitív polinomnak nevezzük, ha az együtthatóinak a legnagyobb közös osztója $1$

## Gauss (Lemma)

Ha $f, g \in \Z[x]$ primitív polinomok, akkor $f g$ is primitív polinom.

### Bizonyítás

Indirekt tfh. $f g$ nem primitív polinom.  
Ekkor van olyan $p \in \Z$ prím, ami osztja $f g$ minden együtthatóját.  
Legyen $i$ illetve $j$ a legkisebb olyan index, amire $p \nmid f_i$ illetve $p \nmid g_j$ (Miért vannak ilyenek?)  
Ekkor $f g$-nek az $(i + j)$ indexű együtthatója $f_0 g_{i + j} + \dots + f_i g_j + \dots + f_{i + j} g_0$ és ebben az
összegben $p$ nem osztója $f_i g_j$-nek, de osztója az összes többi tagnak (Miért?), de akkor nem osztója az összegnek, ami ellentmondás.

## Z feletti polinom felírása primitív polinom segítségével (Állítás)

Minden $0 \ne f \in \Z[x]$ polinom felírható $f = d f^∗$ alakban, ahol $0 \ne d \in \Z$ és $f^∗ \in \Z[x]$ egy primitív polinom.

### Bizonyítás

Ha $f$-ből az együtthatók legnagyobb közös osztóját kiemeljük, és azt $d$-nek választjuk, akkor megkapjuk a megfelelő előállítást.

### Megjegyzés

Az előállítás lényegében (előjelektől eltekintve) egyértelmű, így $f^∗$ főegyütthatóját pozitívnak választva egyértelmű.

## Q feletti polinom feírása primitív polinom segítségével (Állítás)

Minden $0 \ne f \in \text{Q}[x]$ polinom felírható $f = a f^∗$ alakban, ahol $0 \ne a \in \text{Q}$ és $f^∗ \in \Z[x]$ egy primitív polinom.

### Bizonyítás

Írjuk fel $f$ együtthatóit egész számok hányadosaiként.  
Ha végigszorozzuk $f$-et az együtthatói nevezőinek legkisebb közös többszörösével, $c$-vel, majd kiemeljük a kapott $\Z[x]$-beli polinom együtthatóinak $d$ legnagyobb közös osztóját, akkor megkapjuk a megfelelő előállítást $a = d / c$-vel.

### Megjegyzés

Az előállítás lényegében egyértelmű: ha $f^∗$ főegyütthatóját pozitívnak választjuk, akkor egyértelmű.

## Gauss tétele Z[x]-re (Tétel)

Ha egy $f \in Z[x]$ előállítható két nem konstans $g, h \in \text{Q}[x]$ polinom szorzataként, akkor előállítható két nem konstans $g^∗, h^∗ \in \Z[x]$ polinom szorzataként is.

### Bizonyítás

Tfh. $f = g h$ ahol $g, h \in \text{Q}[x]$ nem konstans polinomok.  
Legyen $f = d f^∗$ ahol $d \in \Z$ és $f^∗ \in \Z[x]$ primitív polinom, aminek a főegyütthatója pozitív.  
Ha felírjuk $g$-t $ag'$, $h$-t pedig $bh'$ alakban, ahol $g', h' \in \Z[x]$ primitív polinomok, amiknek a főegyütthatója pozitív, akkor azt kapjuk, hogy  
$d f^∗ = f = g h = a b g' \cdot h'$  
Mivel Gauss
lemmája szerint $g' \cdot h'$ is primitív polinom, továbbá $f$ előállítása primitív polinom segítségével lényegében egyértelmű, ezért $f^∗ = g' h'$ és $d = a b$ vagyis $f = d g'h'$ és például $g^∗ = d g',\ h^∗ = h'$ választással kapjuk $f$ kívánt felbontását.

## Primitív polinom felbonthatóság Z és Q felett (Következmény)

$f \in \Z[x]$ primitív polinom pontosan akkor felbontható $\Z$ fölött, amikor felbontható $\text{Q}$ fölött.

### Bizonyítás

- $\implies$  
Legyen $f \in \Z[x]$ egy primitív polinom, ami felbonható $\Z$ felett.  
Legyen $f = g h$ az $f$ nemtriviális felbontása $\Z$ felett.  
Mivel $f$ primitív, ezért $f$ és $g$ egyike sem konstans, így $f = g h$ nemtriviális felbontás $\text{Q}$ felett is.
- $\impliedby$  
A Gauss-tételből következik az állítás.

## Schönemann-Eisenstein-kritérium (Tétel)

Legyen $f(x) = f_n x^n + f_{n - 1} x^{n - 1} + \dots + f_1 x + f_0 \in \Z[x],\ f_n \ne 0$ legalább elsőfokú primitív polinom.  
Ha található olyan $p \in \Z$ prím, melyre

- $p \nmid f_n$
- $p \mid f_j$ ha $0 \le j < n$
- $p^2 \nmid f_0$

akkor $f$ felbonthatatlan $\Z$ fölött.

### Bizonyítás

Tfh. $f = g h$  
Mivel $p$ nem osztja $f$ főegyütthatóját, ezért sem a $g$, sem a
$h$ főegyütthatóját nem osztja (Miért?).  
Legyen $m$ a legkisebb olyan index, amelyre $p \nmid g_m$ és $o$ a legkisebb olyan index, amelyre $p \nmid h_o$  
Ha $k = m + o$ akkor
$$p \nmid f_k = \sum_{i + j = k} g_i h_j$$
mivel $p$ osztja az összeg minden tagját, kivéve azt, amelyben $i = m$ és $j = o$

Innen $k = n$ ebből pedig $m = \deg(g)$ és $o = \deg(h)$ következik.  
Nem lehetnek $m$ és $o$ egyszerre pozitívak, mert akkor $p \mid g_0$ és $p \mid h_0$ miatt $p^2 \mid g_0 h_0 = f_0$  
következne, ami ellentmondás.  
Így $g$ vagy $h$ konstans polinom, és mivel $f$ primitív, csak konstans $\pm 1$ azaz egység lehet.

### Megjegyzések

- A feltételben $f_n$ és $f_0$ szerepe felcserélhető.
- A tétel nem használható test fölötti polinom irreducibilitásának bizonyítására, mert testben nem léteznek prímek, hiszen minden nem-nulla elem egység.

---

Következő: [Racionális gyökteszt](./racionalis-gyokteszt)
