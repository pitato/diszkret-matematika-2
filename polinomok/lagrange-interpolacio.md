---
title: "Lagrange-interpoláció"
date: "2020-11-15"
tags:
  - Diszkrét Matematika 2
  - Polinomok
---

## Tétel (Lagrange-interpoláció)

Legyen $R$ test, $c_0, c_1, \dots, c_n \in R$ különbözőek, továbbá $d_0, d_1, \dots, d_n \in R$ tetszőlegesek.  
Ekkor létezik egy olyan legfeljebb $n$-ed fokú polinom ($f$), amelyre $f(c_j) = d_j$ ha $j = 0, 1, \dots, n$

### Bizonyítás

Legyen
$$l_j (x) = \frac{\prod_{i \ne j} (x - c_i)}{\prod_{i \ne j} (c_j - c_i)}$$
a $j$-edik Lagrange-interpolációs alappolinom, és legyen
$$f(x) = \sum^n_{j = 0} d_j l_j(x)$$
$l_j (c_i) = 0$ ha $i \ne j$ és $l_j(c_j) = 1$-ből következik az állítás.

### Példa

Adjunk meg olyan $f \in R[x]$ polinomot, amelyre $f(0) = 3, f(1) = 3, f(4) = 7, f(-1) = 0$!  
A feladat szövege alapján $c_0 = 0, c_1 = 1, c_2 = 4, c_3 = -1, d_0 = 3, d_1 = 3, d_2 = 7, d_3 = 0$ értékekkel alkalmazzuk a Lagrange-interpolációt.  
$l_0(x) = \frac{(x - 1)(x - 4)(x + 1)}{(0 - 1)(0 - 4)(0 + 1)} = \frac{1}{4} x^3 - x^2 - \frac{1}{4} x + 1$  
$l_1(x) = \frac{(x - 0)(x - 4)(x + 1)}{(1 - 0)(1 - 4)(1 + 1)} = -\frac{1}{6} x^3 + \frac{1}{2} x^2 + \frac{2}{3} x$  
$l_2(x) = \frac{(x - 0)(x - 1)(x + 1)}{(4 - 0)(4 - 1)(4 + 1)} = \frac{1}{60} x^3 - \frac{1}{60} x$  
$l_3(x) = \frac{(x - 0)(x - 1)(x - 4)}{(-1 - 0)(-1 - 1)(-1 - 4)} = -\frac{1}{10} x^3 + \frac{1}{2} x^2 - \frac{2}{5} x$  
$f(x) = 3 l_0(x) + 3 l_1(x) + 7 l_2(x) + 0 l_3(x) = \frac{22}{60} x^3 - \frac{3}{2} x^2 + \frac{68}{60} x + 3$

|      | $\frac{22}{60}$ | $-\frac{3}{2}$  | $\frac{68}{60}$   | $3$ |     |
| ---- | --------------- | --------------- | ----------------- | --- | --- |
| $1$  | X               | $\frac{22}{60}$ | $-\frac{68}{60}$  | $0$ | $3$ |
| $4$  | X               | $\frac{22}{60}$ | $-\frac{2}{60}$   | $1$ | $7$ |
| $-1$ | X               | $\frac{22}{60}$ | $-\frac{112}{60}$ | $3$ | $0$ |

### Alkalmazás

A Lagrange-interpoláció használható titokmegosztásra a következő
módon:  
legyenek $1 \le m < n$ egészek, továbbá $s \in \N$ a titok, amit $n$ ember között akarunk szétosztani úgy, hogy bármely $m$ részből a titok rekonstruálható legyen, de kevesebből nem.  
Válasszunk a titok maximális lehetséges értékénél és $n$-nél is nagyobb $p$ prímet, továbbá  
$a_1, a_2, \dots, a_{m - 1} \in \Z_p$ véletlen együtthatókat, majd határozzuk meg az  
$f(x) = a_{m - 1} x^{m - 1} + a_{m - 2} x^{m - 2} + \dots + a_1 x + s$ polinomra az $f(i)$ értékeket, és adjuk ezt meg az $i.$ embernek $(i = 1, 2, \dots, n)$  
Bármely $m$ helyettesítési értékből a Lagrange-interpolációval megkapható a polinom, így annak konstans tagja is, a titok.  
Ha $m$-nél kevesebb helyettesítési értékünk van, akkor nem tudjuk meghatározni a titkot, mert tetszőleges $t$ esetén az $f(0) = t$ értéket hozzávéve a többihez létezik olyan legfeljebb $m$-ed fokú polinom, aminek a konstans tagja $t$ és az adott helyeken megfelelő a helyettesítési értéke.

### Példa

Legyen $m = 3, n = 4, s = 5, p = 7$ továbbá $a_1 = 3, a_2 = 4$  
Ekkor  
$f(x) = 4 x^2 + 3x + 5 \in \Z_7[x]$ a titokrészletek pedig  
$f(1) = 5, f(2) = 6, f(3) = 1, f(4) = 4$  
Ha rendelkezünk például az $f(1) = 5, f(3) = 1, f(4) = 4$ információkkal, akkor  
$c_0 = 1, c_1 = 3, c_2 = 4, d_0 = 5, d_1 = 1, d_2 = 4$ értékekkel alkalmazzuk a Lagrange-interpolációt.  
$l_0(x) = \frac{(x - 3)(x - 4)}{(1 - 3)(1 - 4)} = \frac{1}{6}(x^2 - 7x + 12) = \frac{1}{-1}(-6x^2 - 2) = 6x^2 + 2$  
$l_1(x) = \frac{(x - 1)(x - 4)}{(3 - 1)(3 - 4)} = -\frac{1}{2}(x^2 - 5x + 4) = -4(x^2 + 2x + 4) = 3x^2 + 6x + 5$  
$l_2(x) = \frac{(x - 1)(x - 3)}{(4 - 1)(4 - 3)} = \frac{1}{3}(x^2 - 4x + 3) = 5(x^2 + 3x + 3) = 5x^2 + x + 1$  
$f(x) = 5 l_0(x) + l_1(x) + 4 l_2(x) = 30 x^2 + 10 + 3x^2 + 6x + 5 + 20x^2 + 4x + 4 =$  
$= 53x^2 + 10x + 19 = 4x^2 + 3x + 5$

---

Következő: [Polinomok felbonthatósága](./polinomok-felbonthatosaga)
