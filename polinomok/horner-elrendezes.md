---
title: "Horner-elrendezés"
date: "2020-11-14"
tags:
  - Diszkrét Matematika 2
  - Polinomok
---

Legyen $f(x) = f_n x^n + f_{n - 1} x^{n - 1} + \dots +f_1 x + f_0$, ahol $f_n \ne 0$  
Ekkor átrendezéssel a következő alakot kapjuk:  
$f(x) = (\cdots((f_n \cdot x + f_{n - 1})\cdot x + f_{n - 2})\cdot x + \dots + f_1)\cdot x + f_0$ és így  
$f(c) = (\cdots((f_n \cdot c + f_{n - 1})\cdot c + f_{n - 2})\cdot c + \dots + f_1)\cdot c + f_0$

Vagyis $f(c)$ kiszámítható $n$ db szorzás és $n$ db összeadás segítségével.

|     | $f_n$       | $f_{n - 1}$               | $f_{n - 2}$               | $\dots$ | $f_0$                     |                      |
| --- | ----------- | ------------------------- | ------------------------- | ------- | ------------------------- | -------------------- |
| $c$ | X           | $c_1 = f_n$               | $c_2 = c_1 c + f_{n - 1}$ | $\dots$ | $c_n = c_{n - 1} c + f_1$ | $f(c) = c_n c + f_0$ |

Általánosan: $c_k = c_{k - 1} c + f_{n - k + 1}$, ha $1 < k \le n$

Kicsit bőbeszédűbb (de kézzel írva követhetőbb) elrendezésben:

|     | $f_n$       | $f_{n - 1}$               | $f_{n - 2}$               | $\dots$ | $f_1$                     | $f_0$                |
| --- | ----------- | ------------------------- | ------------------------- | ------- | ------------------------- | -------------------- |
| $c$ | X           | $c \cdot c_1$             | $c \cdot c_2$             | $\dots$ | $c \cdot c_{n - 1}$       | $c \cdot c_n$        |
|     | $c_1 = f_n$ | $c_2 = c_1 c + f_{n - 1}$ | $c_3 = c_2 c + f_{n - 2}$ | $\dots$ | $c_n = c_{n - 1} c + f_1$ | $f(c) = c_n c + f_0$ |

### Példák

Határozzuk meg az $f(x) =x^4 - 3 x^3 +x+6$ polinom $-2$ helyen vett
helyettesítési értékét!

|   | $1$ | $-3$ | $0$  | $1$  | $6$   |      |
| - | --- | ---- | ---- | ---- | ----- | ---- |
|   | X   | $1$  | $-5$ | $10$ | $-19$ | $44$ |

Ha az $f(c)$ helyettesítési érték nulla, azaz, ha a $c$ gyöke az $f$ polinomnak, akkor a Horner-elrendezés alsó sorában (a helyettesítési érték előtt) annak a $g$ polinomnak az együtthatói szerepelnek, amire $f(x) = (x - c) \cdot g(x)$

Az $f(x) =x^4 - 4 x^3 + 6 x^2 - 4 x + 1$ polinom $c = 1$ helyen vett helyettesítési értéke nulla:

|   | $1$ | $-4$ | $6$  | $4$  | $1$  |     |
| - | --- | ---- | ---- | ---- | ---- | --- |
|   | X   | $1$  | $-3$ | $3$  | $-1$ | $0$ |

Tehát $f(x) = (x - 1) \cdot (x^3 - 3 x^2 + 3 x - 1)$

---

Következő: [A maradékos osztás tétele, következményei](./maradekos-osztas-tetele-kovetkezmenyei)
