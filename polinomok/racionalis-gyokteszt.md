---
title: "Racionális gyökteszt"
date: "2020-11-15"
tags:
  - Diszkrét Matematika 2
  - Polinomok
---

## Racionális gyökteszt (Tétel)

Legyen $f(x) = f_n x^n + f_{n - 1} x^{n - 1} + \dots + f_1 x + f_0 \in \Z[x],\ f_n \ne 0$ primitív polinom.  
Ha $f(p q) = 0,\ p, q \in \Z,\ (p, q) = 1$ akkor $p \mid f_0$ és $q \mid f_n$

### Bizonyítás

$0 = f (\frac{p}{q}) = f_n (\frac{p}{q})^n + f_{n - 1} (\frac{p}{q})^{n - 1} + \dots + f_1 (\frac{p}{q}) + f_0\ \big/ \cdot q^n$  
$0 = f_n p^n + f_{n - 1} q p^{n - 1} + \dots + f_1 q^{n - 1} p + f_0 q^n$  
$p \mid f_0 q^n$ mivel az összes többi tagnak osztója $p$ és így $(p, q) = 1$ miatt $p \mid f_0$  
$q \mid f_n p^n$ mivel az összes többi tagnak osztója $q$ és így $(p, q) = 1$ miatt $q \mid f_n$

### Megjegyzés

$f$ primitívsége nem szükséges feltétel, csak praktikus. (Miért?)

## Gyök 2 irracionális (Állítás)

$\sqrt{2}$ irracionális, azaz $\sqrt{2} \notin \text{Q}$

### Bizonyítás

Tekintsük az $x^2 - 2 \in \Z[x]$ polinomot.  
Ennek a $p q$ alakú gyökeire ($p, q \in \Z,\ (p, q) = 1$) teljesül, hogy $p \mid 2$ és $q \mid 1$ így a lehetséges racionális gyökei $\pm 1$ és $\pm 2$

---

Következő: [Véges testek](./veges-testek)
