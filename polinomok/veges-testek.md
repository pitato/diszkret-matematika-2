---
title: "Véges testek"
date: "2020-11-15"
tags:
  - Diszkrét Matematika 2
  - Polinomok
---

Tekintsük valamely $p$ prímre a $\Z_p$ testet, továbbá egy $f(x) \in \Z_p[x]$ felbonthatatlan főpolinomot.  
Vezessük be a $g(x) \equiv h(x)\ (\bmod f(x))$ ha $f(x) \mid g(x) - h(x)$ relációt.  
Ez ekvivalenciareláció, ezért meghatároz egy osztályozást $\Z_p[x]$-en.

Minden osztálynak van $\deg(f)$-nél alacsonyabb fokú reprezentánsa
(Miért?), és ha $\deg(g), \deg(h) < \deg(f)$ továbbá $g$ és $h$ ugyanabban az osztályban van, akkor egyenlőek (Miért?).  
Tehát $\deg(f) = n$ esetén bijekciót létesíthetünk az $n$-nél kisebb fokú polinomok és az osztályok között, így $p^n$ darab osztály van.

Az osztályok között értelmezhetjük természetes módon a műveleteket.  
Ezeket végezhetjük az $n$-nél alacsonyabb fokú reprezentánsokkal:  ha a szorzat foka nem kisebb, mint $n$ akkor az $f(x)$-szel vett osztási maradékot vesszük.

$f \nmid g$ esetén a bővített euklideszi algoritmus alapján  
$d(x) = u(x) f(x) + v(x) g(x)$  
Mivel $f(x)$ felbonthatatlan, ezért $d(x) = d$ konstans polinom,  
így $\frac{v(x)}{d}$ multiplikatív inverze lesz $g(x)$-nek.

## Ekvivalenciaosztályok halmazáról (Tétel - nem bizonyított)

Az ekvivalenciaosztályok halmaza a rajta értelmezett összeadással és szorzással testet alkot.

### Megjegyzések

Tetszőleges $p$ prím és $n$ pozitív egész esetén létezik $p^n$ elemű test, mert létezik $n$-ed fokú felbonthatatlan polinom $\Z_p$-ben.

Véges test elemszáma prímhatvány, továbbá az azonos elemszámú testek izomorfak.

### Példa

Tekintsük az $x^2 + 1 \in \Z_3[x]$ felbonthatatlan polinomot. (Miért az?)  
A legfeljebb elsőfokú polinomok: $0, 1, 2, x, x + 1, x + 2, 2x, 2x+ 1, 2x + 2$  
Az összeadás műveleti táblája:

| $+$    | $0$    | $1$    | $2$    | $x$    | $x+1$  | $x+2$  | $2x$   | $2x+1$ | $2x+2$ |
| ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| $0$    | $0$    | $1$    | $2$    | $x$    | $x+1$  | $x+2$  | $2x$   | $2x+1$ | $2x+2$ |
| $1$    | $1$    | $2$    | $0$    | $x+1$  | $x+2$  | $x$    | $2x+1$ | $2x+2$ | $2x$   |
| $2$    | $2$    | $0$    | $1$    | $x+2$  | $x$    | $x+1$  | $2x+2$ | $2x$   | $2x+1$ |
| $x$    | $x$    | $x+1$  | $x+2$  | $2x$   | $2x+1$ | $2x+2$ | $0$    | $1$    | $2$    |
| $x+1$  | $x+1$  | $x+2$  | $x$    | $2x+1$ | $2x+2$ | $2x$   | $1$    | $2$    | $0$    |
| $x+2$  | $x+2$  | $x$    | $x+1$  | $2x+2$ | $2x$   | $2x+1$ | $2$    | $0$    | $1$    |
| $2x$   | $2x$   | $2x+1$ | $2x+2$ | $0$    | $1$    | $2$    | $x$    | $x+1$  | $x+2$  |
| $2x+1$ | $2x+1$ | $2x+2$ | $2x$   | $1$    | $2$    | $0$    | $x+1$  | $x+2$  | $x$    |
| $2x+2$ | $2x+2$ | $2x$   | $2x+1$ | $2$    | $0$    | $1$    | $x+2$  | $x$    | $x+1$  |

Például:  
$2x + 2 + 2x + 1 = 4x + 3 \overset{\Z_3}{=} x$

A szorzás műveleti táblája:

| $\cdot$ | $0$ | $1$    | $2$    | $x$    | $x+1$  | $x+2$  | $2x$   | $2x+1$ | $2x+2$ |
| ------- | --- | ------ | ------ | ------ | ------ | ------ | ------ | ------ | ------ |
| $0$     | $0$ | $0$    | $0$    | $0$    | $0$    | $0$    | $0$    | $0$    | $0$    |
| $1$     | $0$ | $1$    | $2$    | $x$    | $x+1$  | $x+2$  | $2x$   | $2x+1$ | $2x+2$ |
| $2$     | $0$ | $2$    | $1$    | $2x$   | $2x+2$ | $2x+1$ | $x$    | $x+2$  | $x+1$  |
| $x$     | $0$ | $x$    | $2x$   | $2$    | $x+2$  | $2x+2$ | $1$    | $x+1$  | $2x+1$ |
| $x+1$   | $0$ | $x+1$  | $2x+2$ | $x+2$  | $2x$   | $1$    | $2x+1$ | $2$    | $x$    |
| $x+2$   | $0$ | $x+2$  | $2x+1$ | $2x+2$ | $1$    | $x$    | $x+1$  | $2x$   | $2$    |
| $2x$    | $0$ | $2x$   | $x$    | $1$    | $2x+1$ | $x+1$  | $2$    | $2x+2$ | $x+2$  |
| $2x+1$  | $0$ | $2x+1$ | $x+2$  | $x+1$  | $2$    | $2x$   | $2x+2$ | $x$    | $1$    |
| $2x+2$  | $0$ | $2x+2$ | $x+1$  | $2x+1$ | $x$    | $2$    | $x+2$  | $1$    | $2x$   |

Például:

$(2x + 2)(2x + 1) = 4x^2 + 6x + 2 \overset{\Z_3}{=} x^2 + 2 = (x^2 + 1) + 1$

Feladat: Legyen $F_9 = Z_3[x] / (x^2 + 1)$  
Mik lesznek a $z^2 + 1 \in F_9[z]$ polinom gyökei?

---

Következő: [Véges testek](./veges-testek)
