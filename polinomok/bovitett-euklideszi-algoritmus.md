---
title: "Bővített euklideszi algoritmus"
date: "2020-11-15"
tags:
  - Diszkrét Matematika 2
  - Polinomok
---

## Osztó és többszörös polinomgyűrűben (Definíció)

Legyen $R$ kommutatív gyűrű, $f, g \in R[x]$  
Azt mondjuk, hogy $g$ osztója $f$-nek ($f$ többszöröse $g$-nek), ha létezik $h \in R[x]$ amire $f = g \cdot h$

## Polinomok legnagyobb közös osztója (Definíció)

Legyen $R$ kommutatív gyűrű. Az $f, g \in R[x]$ polinomok legnagyobb közös osztójának (kitüntetett közös osztójának) nevezünk egy olyan $d \in R[x]$ polinomot, amelyre $d \mid f, d \mid g$ és $\forall c \in R[x]$ esetén $(c \mid f \land c \mid g)\implies c \mid d$

Testekben minden nem-nulla elem egység, így test fölötti polinomgyűrűben tetszőleges nem-nulla polinommal tudunk maradékosan osztani.  
Ezért működik az euklideszi és a bővített euklideszi algoritmus.  
Ha $R$ test, akkor tetszőleges $f, g \in R[x]$ esetén a bővített euklideszi algoritmus meghatározza $f$ és $g$ (egy) legnagyobb közös osztóját: a $d \in R[x]$ polinomot, továbbá $u, v \in R[x]$ polinomokat, amelyekre $d = u \cdot f + v \cdot g$

## Bővített euklideszi algoritmus test feletti polinomgyűrűben (Tétel)

Legyen $R$ test, $f, g \in R[x]$  
Ha $g = 0$ akkor az $f$ és $g = 0$ polinomoknak $f$ legnagyobb közös osztója és $f = 1 \cdot f + 0 \cdot g$  
Ha $g \ne 0$ akkor végezzük el a következő maradékos osztásokat:

$f = q_1 g + r_1$  
$g = q_2 r_1 + r_2$  
$r 1 = q_3 r_2 +r_3$  
$\vdots$  
$r_{n - 2} = q_n r_{n - 1} + r_n$  
$r_{n - 1} = q_{n + 1} r_n$

Ekkor $d = r_n$ az $f$ és $g$ egy legnagyobb közös osztója.  
Az $u_{-1} = 1,\ u_0 = 0,\ v_{-1} = 0,\ v_0 = 1$ kezdőértékekkel, továbbá az  
$u_i = u_{i - 2} - q_i \cdot u_{i - 1}$ és $v_i = v_{i - 2} - q_i \cdot v_{i - 1}$ rekurziókkal megkapható  
$u = u_n$ és $v = v_n$ polinomokra teljesül $d = u \cdot f + v \cdot g$

### Bizonyítás

A maradékok foka természetes számok szigorúan monoton csökkenő sorozata, ezért az eljárás véges sok lépésben véget ér.  
Indukcióval belátjuk, hogy $r{-1} = f$ és $r_0 = g$ jelöléssel $r_i = u_i \cdot f + v_i \cdot g$ teljesül minden $-1 \le i \le n$ esetén:  
$i = -1$-re: $f = 1 \cdot f + 0 \cdot g$  
$i = 0$-ra: $g = 0 \cdot f + 1 \cdot g$  
Mivel $r_i = r_{i - 2} - q_i \cdot r_{i - 1}$ így az indukciós feltevést használva:  
$r_i = u_{i - 2} \cdot f + v_{i - 2} \cdot g - q_i \cdot (u_{i - 1} \cdot f + v_{i - 1} \cdot g) =$  
$= (u_{i - 2} - q_i \cdot u_{i - 1}) \cdot f + (v_{i - 2} - q_i \cdot v_{i - 1}) \cdot g = u_i \cdot f + v_i \cdot g$  
Tehát $r_n = u_n \cdot f + v_n \cdot g$ és így $f$ és $g$ közös osztói $r_n$-nek is osztói.  
Kell még, hogy $r_n$ osztója $f$-nek és $g$-nek.  
Indukcióval belátjuk, hogy $r_n \mid r_{n - k}$ teljesül minden $0 \le k \le n + 1$ esetén:  
$k = 0$-ra: $r_n \mid r_n$ nyilvánvaló,  
$k = 1$-re $r_{n - 1} = q_{n + 1} r_n$ miatt $r_n \mid r_{n - 1}$  
$r_{n - (k + 1)} = q_{n - (k - 1)} r_{n - k} + r_{n - (k - 1)}$ miatt az indukciós feltevést használva kapjuk az állítást, és így $k = n$ illetve $k = n + 1$ helyettesítéssel  
$r_n \mid r_0 = g$ illetve $r_n \mid r_{-1} = f$

---

Következő: [Polinomok algebrai deriváltjai](./polinomok-algebrai-deriváltjai)
