---
title: "Polinomok alapfogalmai"
date: "2020-11-14"
tags:
  - Diszkrét Matematika 2
  - Polinomok
---

## Polinom, polinomok összege és szorzata (Definíció)

Legyen $(R; +, \cdot)$ gyűrű. A gyűrű elemeiből képzett $f = (f_0, f_1, f_2, \dots)\ (f_j \in \R)$ végtelen sorozatot $R$ fölötti polinomnak nevezzük, ha csak véges sok eleme nem-nulla.  
Az $R$ fölötti polinomok halmazát $R[x]$-szel jelöljük.  
$R[x]$ elemein definiáljuk az összeadást és a szorzást.  
$f = (f_0, f_1, f_2, \dots),\ g = (g_0, g_1, g_2, \dots)$ és $h = (h_0, h_1, h_2, \dots)$ esetén  
$f + g = (f_0 + g_0, f_1 + g_1, f_2 + g_2, \dots)$ és $f \cdot g = h$, ahol  
$$h_k = \sum_{i + j = k} f_i g_j = \sum_{i = 0}^k f_i g_{k - i} = \sum_{j = 0}^k f_{k - j} g_j$$  
Két polinom pontosan akkor egyenlő, ha minden tagjuk egyenlő:  
$f = g \iff \forall j \in \N : f_j = g_j$

### Megjegyzés

Könnyen látható, hogy polinomok összege és szorzata is polinom.

### Jelölés

Az $f = (f_0, f_1, f_2, \dots, f_n, 0, 0, \dots)\ (f_j \in \R), f_n \ne 0\ (f_m = 0 :\forall m > n)$ polinomot  
$f(x) = f_0 + f_1 x + f_2 x^2 + \dots + f_n x^n,\ f_n \ne 0$ alakba írjuk.

## Polinom együtthatói, tagjai, foka (Definíció)

Az előző pontban szereplő polinom esetén $f_i x^i$ a polinom $i$-ed fokú tagja, $f_i$-t az $i$-ed fokú tag együtthatójának nevezzük;  
$f_0$ a polinom konstans tagja, $f_n x^n$ a főtagja, $f_n$ a főegyütthatója.  
A polinom foka $n$, melynek jelölésére $\deg(f)$ használatos.

### Példa

Az $f = (1, 0, 2, 0, 0, 3, 0, \dots)$ polinom felírható  
$f(x) = 1 + 0 x + 2 x^2 + 0 x^3 + 0 x^4 + 3 x^5$ alakban.  
Ugyanezen $f$ további alakjai:  
$f(x) = 1 + 2 x^2 + 3 x^5,\ f(x) = 3 x^5 + 2 x^2 + 1$

### Megjegyzés

A főegyüttható tehát a legnagyobb indexű nem-nulla együttható, a fok
pedig ennek indexe.  
A $0 = (0, 0, \dots)$ nullpolinomnak nincs legnagyobb indexű nem-nulla együtthatója, így a fokát külön definiáljuk, mégpedig $\deg(0) = -\infin$

## Konstans polinomok, lineáris polinomok (Definíció)

A konstans polinomok a legfeljebb nulladfokú polinomok, a lineáris
polinomok pedig a legfeljebb elsőfokú polinomok.  
Az $f_i x^i$ alakba írható polinomok a monomok.  
Ha $f \in \R[x]$ polinom főegyütthatója $R$ egységeleme, akkor $f$-et főpolinomnak nevezzük.

### Példák

- $x^3 + 1 \in \Z[x]$
- $\frac{2}{3} \in \text{Q}[x]$
- $\pi x + (i + \sqrt{2}) \in \cnums[x]$

## Halmaz fölötti polinomgyűrű (Definíció-Állítás)

Ha $(R; +, \cdot)$ gyűrű, akkor $(R[x]; + , \cdot)$ is gyűrű, és $R$ fölötti polinomgyűrűnek nevezzük.

### Megjegyzés

Gyakran az $(R; +, \cdot)$ gyűrűre szimplán $R$-ként, az $(R[x]; +, \cdot)$ gyűrűre $R[x]$-ként hivatkozunk.

## Kommutatív gyűrű feletti polinomgyűrű (Állítás)

Ha az $R$ gyűrű kommutatív, akkor $R[x]$ is kommutatív.

### Bizonyítás

$(f \cdot g) k = f_0 g_k + f_1 g_{k - 1} + \dots + f_{k - 1} g_1 + f_k g_0 =$  
$= g_k f_0 + g_{k - 1} f_1 + \dots + g_1 f_{k - 1} + g_0 f_k =$  
$= g_0 f_k + g_1 f_{k - 1} + \dots + g_{k - 1} f_1 + g_k f_0 = (g \cdot f)_k$

## Egységelemes gyűrű feletti polinomgyűrű egységeleme (Állítás)

$1 \in R$ egységelem esetén $e = (1, 0, 0 \dots)$ egységeleme lesz $R[x]$-nek.

### Bizonyítás

$$(f \cdot e)_k = \sum_{j = 0}^k f_j e_{k - j} = \sum_{j = 0}^{k - 1} f_j e_{k - j} + f_k e_0 = f_k$$

## Nullosztómentes gyűrű feletti polinomgyűrű (Állítás)

Ha az $R$ gyűrű nullosztómentes, akkor $R[x]$ is nullosztómentes.

### Bizonyítás

Legyen $n$, illetve $m$ a legkisebb olyan index, amire $f_n \ne 0$, illetve $g_m \ne 0$  
$(f \cdot g)_{n + m} = \sum^{n + m}_{j = 0} f_j g_{n + m - j} = \sum^{n - 1}_{j = 0} f_j g_{n + m - j} + f_n g_m + \sum^{n + m}_{j = n + 1} f_j g_{n + m - j} =$  
$= 0 + f_n g_m + 0 = f_n g_m \ne 0$

## Polinomok összegének, ill. szorzatának foka (Állítás)

Legyen $f, g \in \R[x],\ \deg(f) = n, \deg(g) = k$  
Ekkor:

- $\deg(f + g) \le \max(n, k)$
- $\deg(f \cdot g) \le n + k$

### Bizonyítás

Legyen $h = f + g$  
Ekkor $j > \max(n, k)$ esetén $h_j = 0 + 0 = 0$  
Legyen $h = f \cdot g$  
Ekkor $j > n + k$ esetén
$$h_j = \sum^j_{i = 0} f_i g_{j - i} = \sum^n_{i = 0} f_i g_{j - i} + \sum^j_{i = n + 1} f_i g_{j - i} = \sum^n_{i = 0} f_i \cdot 0 + \sum^j_{i = n + 1} 0 \cdot g_{j - i} = 0$$

### Megjegyzés

Nullosztómentes gyűrű esetén egyenlőség teljesül a 2. egyenlőtlenségben, hiszen
$$h_{n + k} = \sum^{n + k}_{i = 0} f_i g_{n + k - i} = \sum^{n - 1}_{i = 0} f_i g_{n + k - i} + f_n g_k + \sum^{n + k}_{i = n + 1} f_i g_{n + k - i} = f_n g_k \ne 0$$

## Helyettesítési érték (Definíció)

Az $f(x) = f_0 + f_1 x + f_2 x^2 + \dots + f_n x^n \in \R[x]$ polinom $r \in \R$ helyen felvett helyettesítési értékén az $f(r) = f_0 + f_1 r + f_2 r^2 + \dots + f_n r^n \in \R$ elemet értjük.

## Gyök (Definíció)

$f(r) = 0$ esetén $r$-et a polinom gyökének nevezzük.

### Példa

$f(x) = x^2 + x - 2 \in \Z[x]$-nek a $-2$ helyen felvett helyettesítési értéke  
$(- 2)^2 + (- 2) - 2 = 0$, ezért $-2$ gyöke $f$-nek.

## Polinomfüggvény (Definíció)

Az $\hat{f}: r \rightarrow f(r)$ leképezés az $f$ polinomhoz tartozó polinomfüggvény.

Másik tárgyban lehet, hogy az itt "polinomfüggvénynek" nevezett dolgot hívtátok "polinomnak", és bizonyos esetekben ez nem is okoz gondot, de ebben a tárgyban gondosan ügyeljetek a két fogalom közötti különbségre!

### Megjegyzés

Ha $R$ véges, akkor csak véges sok $R \rightarrow R$ függvény van, míg végtelen sok $R[x]$-beli polinom, így vannak olyan polinomok, amikhez ugyanaz a polinomfüggvény tartozik, például $x, x^2 \in \Z_2 [x]$

### Megjegyzés

Ha $R$ végtelen elemszámú nullosztómentes gyűrű, akkor hiába van végtelen sok $R \rightarrow R$ függvény és végtelen sok $R[x]$-beli polinom, mégis lesznek olyan függvények, amik nem tartozhatnak egyetlen polinomhoz sem annak polinomfüggvényeként.

---

Következő: [Horner-elrendezés](./horner-elrendezes)
