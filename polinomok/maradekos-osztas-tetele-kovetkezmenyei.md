---
title: "A maradékos osztás tétele, következményei"
date: "2020-11-14"
tags:
  - Diszkrét Matematika 2
  - Polinomok
---

## Polinomok maradékos osztása (Tétel)

Legyen $R$ egységelemes integritási tartomány, $f , g \in R[x]$ és tegyük fel, hogy $g$ főegyütthatója egység $R$-ben.  
Ekkor egyértelműen léteznek olyan $q, r \in R[x]$ polinomok, melyekre $f = q g + r$, ahol $\deg(r) < \deg(g)$

A fenti tétel az $f$ polinomnak a $g$ polinommal való maradékos elosztásának az egyértelmű elvégezhetőségét mondja ki.  
A $q$ polinomot a maradékos osztás hányadospolinomjának, az $r$ polinomot az osztás maradékpolinomjának nevezzük.

### Bizonyítás

- Létezés: $f$ foka szerinti teljes indukció:  
Ha $\deg(f) < \deg(g)$, akkor $q = 0$ és $r = f$ esetén megfelelő előállítást kapunk.  
Tfh. $\deg(f) \ge \deg(g)$  
Legyen $f$ főegyütthatója $f_n$, $g$ főegyütthatója $g_k$  
Mivel $g_k$ egység, ezért $\exists g_k' \in R$, melyre $f_n = g_k' g_k$  
Ekkor $g_k' x^{n - k} g(x)$ és $f(x)$ főtagja megegyezik, így $f^∗(x) = f(x) - g_k' x^{n - k} g(x)$-re $\deg(f^∗) < \deg(f)$ (Miért?)  
Így $f^∗$-ra használhatjuk az indukciós feltevést, vagyis léteznek $q^∗, r^∗ \in R[x]$ polinomok, melyekre $f^∗ = q^∗ g + r^∗$ és $\deg(r^∗) < \deg(g)$  
Ekkor $f(x) = f^∗(x) + g_k' x^{n - k} g(x) = q^∗(x) g(x) +r^∗(x) + g_k' x^{n - k} g(x) = (q^∗(x) + g_k' x^{n - k}) g(x) + r^∗(x)$  
így $q(x) = q^∗(x) + g_k' x^{n - k}$ és $r(x) = r^∗(x)$ jó választás.
- Egyértelműség: Tekintsük $f$ két megfelelő előállítását:  
$f = qg + r = q^∗ g + r^∗$ amiből:  
$g(q - q^∗) = r^∗ - r$  
A jobb oldali polinom foka:  
$\deg(r^∗ - r) \le \max \{\deg(r^∗), \deg(-r)\} = \deg(r^∗) < \deg(g)$  
A bal oldali polinom foka: $\deg(g (q - q^∗)) = \deg(g) + \deg(q - q^∗)$, ami csak akkor kisebb mint $\deg(g)$, ha $\deg(q - q^∗) < 0$, azaz, ha $q - q^∗$ a nullpolinom, tehát, ha $q = q^∗$, akkor $r = r^∗$ is következik.

## Gyöktényező (Definíció)

Ha $c\in R$ az $f \in R[x]$ polinom gyöke, akkor $(x - c) \in R[x]$ a $c$-hez tartozó gyöktényező.

## Gyöktényező leválasztása (Következmény)

Legyen $R$ egységelemes integritási tartomány.  
Ha $f \in R[x]$, és $c \in R$ gyöke $f$-nek, akkor létezik olyan $q \in R[x]$, amelyre $f(x) = (x - c) q(x)$.

### Bizonyítás

Osszuk el maradékosan $f$-et $(x - c)$-vel (Miért lehet?):  
$f(x) = q(x) (x - c) + r (x)$  
Mivel $\deg(r(x)) < \deg(x-c) =1$, ezért $r$ konstans polinom.  
Helyettesítsünk be $c$-t, így azt kapjuk, hogy  
$0 = f(c) = q(c) (c - c) + r(c) = r(c) = r$ tehát $r = 0$

## Egységelemes integritási tartomány feletti polinom gyökeinek száma (Következmény)

Az $R$ egységelemes integritási tartomány fölötti $f \ne 0$ polinomnak legfeljebb $\deg(f)$ gyöke van.

### Bizonyítás

$f$ foka szerinti teljes indukció:
$\deg(f) = 0$ esetén igaz az állítás. (Miért?)  
Ha $\deg(f) > 0$ és $f(c) = 0$ akkor $f(x) = (x - c) g(x)$ (Miért?), ahol  
$\deg(g) = \deg(f) - 1$ (Miért?).  
Ha $d$ gyöke $f$-nek, akkor $0 = f(d) = (d - c) g(d)$ azaz (Miért is?) vagy $d - c = 0$ (amiből $d = c$), vagy $g(d) = 0$ azaz $d$ gyöke $g$-nek.  
Az indukciós feltevés szerint $g$-nek legfeljebb $\deg(g) = \deg(f) - 1$ gyöke van, ahonnan az állítás következik.

Ha $R$ gyűrű NEM egységelemes integritási tartomány (például azért, mert vannak benne nullosztók), akkor nem igaz a fenti állítás.  
Például $\Z_6$ fölött:  
$(x - 2)(x - 3) \equiv x^2 + x \equiv (x - 0)(x + 1)\ (\bmod 6)$

## n + 1 helyen megegyező legfeljebb n fokú polinomok (Következmény)

Ha $R$ egységelemes integritási tartomány, akkor ha két, legfeljebb $n$-ed fokú $R[x]$-beli polinomnak $n + 1$ különböző helyen ugyanaz a helyettesítési értéke, akkor egyenlőek.

### Bizonyítás

A két polinom különbsége legfeljebb $n$-ed fokú, és $n + 1$ gyöke van (Miért?), ezért nullpolinom (Miért?), vagyis a polinomok egyenlőek.

## Polinomok és polinomfüggvények kapcsolata végtelen egységelemes integritási tartomány felett (Következmény)

Ha $R$ végtelen egységelemes integritási tartomány, akkor két különböző $R[x]$-beli polinomhoz nem tartozik ugyanaz a polinomfüggvény.

### Bizonyítás

Ellenkező esetben a polinomok különbségének végtelen sok gyöke lenne (Miért?).

---

Következő: [Bővített euklideszi algoritmus](./bovitett-euklideszi-algoritmus)
