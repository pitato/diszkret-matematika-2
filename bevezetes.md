---
title: "Bevezetés - Diszkrét Matematika 2"
date: "2020-11-11"
tags:
  - Diszkrét Matematika 2
---

Juhász Zsófia diái alapján készült.

## Tárgy információk

**Tárgy:** Diszkrét Matematika 2  
Prog. inf. BSc / B  
**Kód:** IP-18bDM2E, IP-18bDM2G  
**Félév:** 2020/2021 ősz

## Tematika

- Számelmélet
- Algebra és Polinomok
- Kódolás [**nincs a jegyzetekben**]

## Kedvcsináló

"Isten megteremtette a természetes számokat, minden más az ember műve." (Leopold Kroenecker, 1823 – 1891)

A számelmélet az egész számok tulajdonságaival foglalkozik.
Alkalmazásai:

- kriptográfia: nyilvános kulcsú rejtjelezés
- kódolás: hibajavító kódok
- számítógépes számelmélet
- sok eredménye általánosítható más struktúrákra: más számhalmazokra, polinomgyűrűkre, ill. ún. egységelemes integritási tartományokra

## Áttekintés: Mit tanulunk Számelméletből

A teljesség igénye nélkül:

- Alapfogalmak, például:
  - oszthatóság és alaptulajdonságai, legnagyobb közös osztó (lnko), legkisebb közös többszörös (lkkt), irreducibilis számok, prímek, maradékos osztás és következményei...
  - (Bővített) euklideszi algoritmus és következményei
  - Számelmélet alaptétele, kanonikus alak, lnko és lkkt meghatározása a kanonikus alakból, Euler-féle $\varphi$-függvény
- Diofantikus egyenletek: kétváltozós lineáris diofantikus egyenletek megoldása
- Kongruenciák: kongruenciák és alaptulajdonságaik, maradékosztályok, egyváltozós lineáris kongruenciák megoldása, szimultán kongruenciarendszerek megoldása a Kínai maradéktétel segítségével
- Euler-Fermat tétel és egy alkalmazása: az RSA nyilvános kulcsú rejtjelező algoritmus
- Prímekről: néhány klasszikus eredmény, bizonyítás nélkül

## Néhány összefüggés tanult számelméleti tételek között

![összefüggések](osszefuggesek.drawio.png)

---

Következő: [Számelmélet - Oszthatóság](./szamelmelet/oszthatosag)
